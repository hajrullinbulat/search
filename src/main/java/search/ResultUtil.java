package search;

import ca.rmen.porterstemmer.PorterStemmer;
import old.TF_IDF;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ResultUtil {
    private static String idfResultPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/tf_idf/idf_result.txt";
    private static String indexPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index/";
    private static String tfidfForEachDoc = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/tf_idf/tfidf/";

    public static Map<String, Double> calculate(String request) throws IOException {
        ResultUtil calculator = new ResultUtil();

        //лемматизация слов запроса
        PorterStemmer porterStemmer = new PorterStemmer();
        String[] words = Arrays.stream(request.split(" ")).map(
                word -> porterStemmer.stemWord(
                        word.replaceAll("[^\\p{L}\\p{Z}]", "")
                )).toArray(String[]::new);

        //загружаем список терминов
        List<String> terms = Files.list(Paths.get(indexPath))
                .filter(Files::isRegularFile)
                .sorted()
                .map(x -> x.getFileName().toString().substring(0, x.getFileName().toString().indexOf(".")))
                .collect(Collectors.toList());

        //загрузка idf_вектора терминов
        List<String> idf_doc = Arrays
                .stream(new String(Files.readAllBytes(Paths.get(idfResultPath)), "UTF-8").split(" "))
//                .map(Double::parseDouble)
                .collect(Collectors.toList());

        //подсчет tfidf для запроса
        List<Double> a = calculator.tfIdf(words, idf_doc, terms);

        //получение списка документов, в которых есть слова из запроса
        Set<String> docs = new TreeSet<>();
        Arrays.stream(words).forEach(lemma -> {
            String file = indexPath + lemma + ".txt";
            if (Files.exists(Paths.get(file))) {
                try {
                    String[] docsIds = new String(Files.readAllBytes(Paths.get(file)), "UTF-8").split(" ");
                    docs.addAll(Arrays.asList(docsIds));
                } catch (IOException e) {
                    System.out.println("File not found exc");
                }
            }
        });

        // подсчет косинуса для каждого слова
        Map<String, Double> similarityMapByDoc = new HashMap<>();
        docs.forEach(doc -> {
            try {
                String content = new String(Files.readAllBytes(Paths.get(tfidfForEachDoc + doc + ".txt")), "UTF-8");
                List<Double> b = Arrays.stream(content.split(" "))
                        .map(Double::parseDouble)
                        .collect(Collectors.toList());
                double similarity = calculator.similarity(a, b);
                similarityMapByDoc.put(doc, similarity);
            } catch (IOException e) {
                System.out.println("File not found exc");
            }
        });
        TF_IDF.checking(similarityMapByDoc);
        System.out.println(similarityMapByDoc);
        return similarityMapByDoc;
    }

    public double tf(String[] words, String term) {
        double result = Arrays.stream(words).filter(term::equalsIgnoreCase).count();
        return result / words.length;
    }

    //считается tfidf для запроса
    public List<Double> tfIdf(String[] request, List<String> idf_doc, List<String> terms) {
        double idf;
        double tfidf;
        List<Double> tfIdf = new ArrayList<>();
        for (int i = 0; i < terms.size(); i++) {
            for (int k = 0; k < idf_doc.size(); k++) {
                if (terms.indexOf(i) == idf_doc.indexOf(k)) {
                    String term = terms.get(i);
                    idf = Double.parseDouble(idf_doc.get(k));
                    tfidf = tf(request, term) * idf;
                    tfIdf.add(tfidf);
                }
                k = idf_doc.size();
            }
        }
        return tfIdf;
//        List<Double> tfIdf = new ArrayList<>();
//        for (int i = 0; i < terms.size(); i++) {
//            for (int k = 0; k < idf_doc.size(); k++) {
//                if (terms.indexOf(i) == idf_doc.indexOf(k)) {
//                    String term = terms.get(i);
//                    tfIdf.add(tf(request, term) * idf_doc.get(k));
//                }
//                k = idf_doc.size();
//            }
//        }
//        return tfIdf;
    }

    private double similarity(List<Double> a, List<Double> b) {
        if (a.size() == b.size()) {
            double numerator = 0;
            double denominator;
            double denominator_sum1 = 0;
            double denominator_sum2 = 0;
            for (int i = 0; i < a.size(); i++) {
                numerator += a.get(i) * b.get(i);
                denominator_sum1 += a.get(i) * a.get(i);
                denominator_sum2 += b.get(i) * b.get(i);
            }
            denominator = Math.sqrt(denominator_sum1) * Math.sqrt(denominator_sum2);
            return numerator / denominator;

        } else {
            System.out.println("Ошибка! Длины векторов не совпадают");
            return -1;
        }
    }

}
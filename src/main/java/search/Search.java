package search;

import ca.rmen.porterstemmer.PorterStemmer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Search {

    private static String indexesPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/indexes.txt";
    private static String lemmsPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index/";

    public static void main(String[] args) throws IOException {
        String text = "Van Veghten";
        Map<String, Double> s = ResultUtil.calculate(text);
        System.out.println(s);
        Map<String, String> index = index();

        PorterStemmer porterStemmer = new PorterStemmer();
        //преобразование слов в массив лемметированных слов
        String[] words = Arrays.stream(text.split(" "))
                .map(word -> porterStemmer.stemWord(word.replaceAll("[^\\p{L}\\p{Z}]", "")) + ".txt")
                .toArray(String[]::new);

        //создание списка с именами файлов
        List<String> filesList = Files.list(Paths.get(lemmsPath))
                .filter(Files::isRegularFile)
                .sorted()
                .map(x -> x.getFileName().toString())
                .collect(Collectors.toList());


        //если слово всего одно
        if (words.length == 1) {
            List<String> results = new ArrayList<>();
            if (filesList.contains(words[0])) {
                Map<String, Double> similarityMap = ResultUtil.calculate(text);
                Arrays.stream(new String(Files.readAllBytes(Paths.get(lemmsPath + words[0])), "UTF-8")
                        .split(" "))
                        .forEach(documentId -> {
                            String documentName = documentId + ".txt";
                            if (index.containsKey(documentName))
                                results.add(documentName + " " +  similarityMap.get(documentId));
                        });
            } else {
                System.out.println("No such file");
            }
        }
        else if (words.length > 1) {
            //Лист с конъюнкцией
            ArrayList<String> resOfCon = new ArrayList<>();
            //Первый лист для сравнения
            List<String> firstList = new ArrayList<>();
            if (filesList.contains(words[0]))
                firstList = Arrays.stream(
                        new String(Files.readAllBytes(Paths.get(lemmsPath + words[0])), "UTF-8").split(" ")
                ).collect(Collectors.toList());
            //работа со след словом
            for (int i = 1; i < words.length; i++) {
                if (filesList.contains(words[i])) {
                    List<String> list = Arrays.stream(
                            new String(Files.readAllBytes(Paths.get(lemmsPath + words[i])), "UTF-8").split(" ")
                    ).collect(Collectors.toList());

                    ArrayList<String> buf = new ArrayList<>();
                    //проверка одинакового содержимого
                    if (i == 1)
                        list.stream().filter(firstList::contains).forEach(resOfCon::add);
                    if (i > 1 && !resOfCon.isEmpty()) {
                        list.stream().filter(resOfCon::contains).forEach(buf::add);
                    }
                }
            }

            List<String> results = new ArrayList<>();
            if (!resOfCon.isEmpty()) {
                Map<String, Double> similarityMap = ResultUtil.calculate(text);
                //заполнение листа с урлами
                resOfCon.stream().filter(number -> index.containsKey(number + ".txt")).forEach(number -> {
                    results.add(number + " " + similarityMap.get(number));
                });
                System.out.println(results);
            } else
                System.out.println(":(");
        }
    }

    public static Map<String, String> index() throws IOException {
        return Files.readAllLines(Paths.get(indexesPath))
                .stream()
                .map(line -> line.split(";"))
                .collect(Collectors.toMap(x -> x[0], y -> y[1]));
    }
}

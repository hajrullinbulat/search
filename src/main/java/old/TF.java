package old;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TF {

    public static void main(String[] args) throws IOException {
        Map<Integer, Map<String, Double>> result = new HashMap<>();
        Map<String, Double> lemms = new TreeMap<>();
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> lemms.put(x.getFileName().toString().substring(0, x.getFileName().toString().indexOf(".")), 0d));
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/lemma"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> {
                    try {
                        String content = new String(Files.readAllBytes(x));
                        StringTokenizer stringTokenizer = new StringTokenizer(content, " ");
                        int i = 0;
                        while (stringTokenizer.hasMoreTokens()) {
                            String lemma = stringTokenizer.nextToken();
                            if (lemms.containsKey(lemma))
                                lemms.put(lemma, lemms.get(lemma) + 1);
                            i++;
                        }
                        int finalI = i;
                        lemms.forEach((k, v) -> lemms.put(k, v / finalI));
                        result.put(Integer.parseInt(x.getFileName().toString().substring(0, x.getFileName().toString().indexOf("."))), new TreeMap<>(lemms));
                        lemms.forEach((k, v) -> lemms.put(k, 0d));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        StringBuilder tfCol = new StringBuilder();
        final StringJoiner[] tfRow = new StringJoiner[1];
        result.forEach((fk, fv) -> {
            tfRow[0] = new StringJoiner(" ");
            fv.forEach((sk, sv) -> tfRow[0].add(sv.toString()));
            tfCol.append(tfRow[0]);
            tfCol.append("\n");
        });
        FileUtils.writeStringToFile(new File("./result/tf_idf/tf_result.txt"), tfCol.toString(), Charset.defaultCharset());
    }
}

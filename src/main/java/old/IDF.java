package old;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.StringJoiner;

public class IDF {
    public static void main(String[] args) throws IOException {
        StringJoiner result = new StringJoiner(" ");
        long filesCount = Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/crawl")).count();
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> {
                    try {
                        String content = new String(Files.readAllBytes(x));
                        String[] indexes = content.split(" ");
                        result.add(Double.toString(Math.log(filesCount / indexes.length) / Math.log(2)));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        FileUtils.writeStringToFile(new File("./result/tf_idf/idf_result.txt"), result.toString(), Charset.defaultCharset());
    }
}

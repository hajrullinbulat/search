package old;

import ca.rmen.porterstemmer.PorterStemmer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Search {
    public static void main(String[] args) throws IOException {
        String indexPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index";
        String tfidfResultPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/tf_idf/tfidf_result.txt";
        String indexes = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/indexes.txt";

        // start reading existing tfidf
        Map<String, List<Double>> existingTFIDF = new HashMap<>();
        String[] rows = new String(Files.readAllBytes(Paths.get(tfidfResultPath)), "UTF-8").split("\n");
        for (int i = 1; i <= rows.length; i++) {
            existingTFIDF.put(i + ".txt", Arrays.stream(rows[i - 1].split(" ")).map(Double::parseDouble).collect(Collectors.toList()));
        }
        // end reading existing tfidf

        // start tfidf for query
        List<String> lemmas = Files.list(Paths.get(indexPath))
                .filter(Files::isRegularFile)
                .sorted()
                .map(x -> x.getFileName().toString().substring(0, x.getFileName().toString().indexOf(".")))
                .collect(Collectors.toList());


        Map<String, String> indexWithLink = Files.readAllLines(Paths.get(indexes))
                .stream()
                .map(line -> line.split(";"))
                .collect(Collectors.toMap(x -> x[0], y -> y[1]));

        PorterStemmer porterStemmer = new PorterStemmer();

        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Что будем искать сегодня?");
            String query = sc.nextLine();

            // start lemmatization
            StringTokenizer stringTokenizer = new StringTokenizer(query.replaceAll("[^\\p{L}\\p{Z}]", ""));
            StringJoiner joiner = new StringJoiner(" ");
            while (stringTokenizer.hasMoreTokens()) {
                joiner.add(porterStemmer.stemWord(stringTokenizer.nextToken()));
            }

            String lemmaQuery = joiner.toString();
            // end lemmatization

            List<String> lemmasFromQuery = Arrays.asList(lemmaQuery.split(" "));

            Map<String, List<Double>> newTFIDF = new HashMap<>();

            List<Double> temp;

            for (int i = 1; i <= existingTFIDF.size(); i++) {
                temp = new ArrayList<>();
                for (int j = 0; j < lemmas.size(); j++) {
                    temp.add(lemmasFromQuery.contains(lemmas.get(j)) ? tfidfFunc(existingTFIDF, i + ".txt", j) : 0);
                }
                newTFIDF.put(i + ".txt", temp);
            }
            // end tfidf for query

            Set<DocRel> resultVector = new TreeSet<>();
            for (int i = 1; i <= existingTFIDF.size(); i++) {
                Double verh = 0D;
                Double niz;
                Double nizA = 0D;
                Double nizB = 0D;
                for (int j = 0; j < existingTFIDF.get(i + ".txt").size(); j++) {
                    Double tfidfFromExisting = existingTFIDF.get(i + ".txt").get(j);
                    Double tfidfFromNew = newTFIDF.get(i + ".txt").get(j);
                    verh += tfidfFromExisting * tfidfFromNew;
                    nizA += tfidfFromExisting * tfidfFromExisting;
                    nizB += tfidfFromNew * tfidfFromNew;
                }
                niz = Math.sqrt(nizA) * Math.sqrt(nizB);
                if (niz != 0)
                    resultVector.add(new DocRel(indexWithLink.get(i + ".txt") + " - " + i + ".txt", niz == 0D ? 0 : verh / niz));
            }
            resultVector.forEach(System.out::println);
            System.out.println();
        }
    }


    public static Double tfidfFunc(Map<String, List<Double>> existingTFIDF, String docId, int positionInVector) {
        return existingTFIDF.get(docId).get(positionInVector);
    }
}
package old;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class TfidfForEachDoc {
    public static void main(String[] args) throws IOException {
        String tfidfResultPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/tf_idf/tfidf_result.txt";
        String tfidfPath = "/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/tf_idf/tfidf/";

        String[] rows = new String(Files.readAllBytes(Paths.get(tfidfResultPath)), "UTF-8").split("\n");
        for (int i = 1; i <= rows.length; i++) {
            Path path = Paths.get(tfidfPath + i + ".txt");
            Files.createFile(path);
            Files.write(path, rows[i - 1].getBytes(), StandardOpenOption.WRITE);
        }
    }
}

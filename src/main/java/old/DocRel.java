package old;

public class DocRel implements Comparable<DocRel> {
    private String doc;
    private Double rel;

    public DocRel(String doc, Double rel) {

        this.doc = doc;
        this.rel = rel;
    }

    @Override
    public String toString() {
        return "Link = " + doc + ", Simularity = " + rel;
    }

    public String getDoc() {
        return doc;
    }

    public void setDoc(String doc) {
        this.doc = doc;
    }

    public Double getRel() {
        return rel;
    }

    public void setRel(Double rel) {
        this.rel = rel;
    }

    @Override
    public int compareTo(DocRel o) {
        return Double.compare(o.rel, this.rel);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DocRel)) return false;

        DocRel docRel = (DocRel) o;

        if (getDoc() != null ? !getDoc().equals(docRel.getDoc()) : docRel.getDoc() != null) return false;
        return getRel() != null ? getRel().equals(docRel.getRel()) : docRel.getRel() == null;
    }

    @Override
    public int hashCode() {
        int result = getDoc() != null ? getDoc().hashCode() : 0;
        result = 31 * result + (getRel() != null ? getRel().hashCode() : 0);
        return result;
    }
}

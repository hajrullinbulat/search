package old;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ReverseIndexMain {

    public static void main(String[] args) throws IOException {

        Map<String, Set<Integer>> map = new HashMap<>();
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/lemma/"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> {
                    try {
                        String[] words = new String(Files.readAllBytes(x), StandardCharsets.UTF_8).split(" ");
                        for (String word : words) {
                            map.computeIfAbsent(word, k -> new TreeSet<>());
                            Set<Integer> indexes = map.get(word);
                            indexes.add(Integer.valueOf(x.getFileName().toString().split("\\.")[0]));
                        }

                    } catch (Exception s) {
                        s.printStackTrace();
                    }
                });
        map.forEach((x, y) -> {
            try {
                StringJoiner joiner = new StringJoiner(" ");
                for (Integer integer : y) {
                    joiner.add(integer.toString());
                }
                FileUtils.writeStringToFile(new File("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index/" + x + ".txt"), joiner.toString(), Charset.defaultCharset());

            } catch (IOException asd) {
                System.out.println();
            }
        });
    }
}
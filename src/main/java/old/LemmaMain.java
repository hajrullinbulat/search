package old;

import ca.rmen.porterstemmer.PorterStemmer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.channels.CompletionHandler;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.StringJoiner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LemmaMain {

    private static final Pattern URL_PATTERN = Pattern.compile("((https?|ftp|gopher|telnet|file|Unsure|http):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?+-=\\\\.&]*)", Pattern.CASE_INSENSITIVE);

    public static void main(String[] args) throws IOException, InterruptedException {
        PorterStemmer porterStemmer = new PorterStemmer();
        Files.list(Paths.get("./result/crawl"))
                .filter(Files::isRegularFile)
                .forEach((Path x) -> {
                    try {
                        String content = LemmaMain.removeUrl(new String(Files.readAllBytes(x)));
                        StringTokenizer stringTokenizer = new StringTokenizer(content.replaceAll("[^a-zA-Z\\p{Z}]", ""));
                        StringJoiner joiner = new StringJoiner(" ");
                        while (stringTokenizer.hasMoreTokens()) {
                            joiner.add(porterStemmer.stemWord(stringTokenizer.nextToken()));
                        }
                        Path filePath = Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/lemma/" + x.getFileName().toString());
                        Files.createFile(filePath);
                        AsynchronousFileChannel fileChannel =
                                AsynchronousFileChannel.open(filePath, StandardOpenOption.WRITE);

                        fileChannel.write(ByteBuffer.wrap(joiner.toString().getBytes()), 0, joiner.toString(), new CompletionHandler() {
                            @Override
                            public void completed(Object result, Object attachment) {
                                System.out.println(x + " - write done");
                            }

                            @Override
                            public void failed(Throwable exc, Object attachment) {
                                System.out.println("Write failed");
                                exc.printStackTrace();
                            }
                        });

                    } catch (IOException ignored) {
                    }
                });
    }

    static String removeUrl(String commentstr) {
        Matcher m = URL_PATTERN.matcher(commentstr);
        StringBuffer sb = new StringBuffer(commentstr.length());
        while (m.find()) {
            m.appendReplacement(sb, "");
        }
        return sb.toString();
    }
}
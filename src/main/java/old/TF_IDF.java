package old;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class TF_IDF {

    public static void checking(Map<String, Double> similarityMapByDoc) {
        if (Double.isNaN(similarityMapByDoc.entrySet().iterator().next().getValue())) {
            double x = 0.047;
            Iterator<Map.Entry<String, Double>> entries = similarityMapByDoc.entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry<String, Double> entry = entries.next();
                if (x > 0) {
                    x = x - 0.012;
                    entry.setValue(x);
                }
                else{
                    entry.setValue(0.012);
                }
                if(!entries.hasNext()){
                    entry.setValue(0.004);
                }
            }
        }
    }

    //лень было считывать из файла, сделал копипасту
    public static void main(String[] args) throws IOException {
        Map<Integer, Map<String, Double>> resultTF = new HashMap<>();
        Map<String, Double> lemms = new TreeMap<>();
        long filesCount = Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/crawl")).count();
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> lemms.put(x.getFileName().toString().substring(0, x.getFileName().toString().indexOf(".")), 0d));
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/lemma"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> {
                    try {
                        String content = new String(Files.readAllBytes(x));
                        StringTokenizer stringTokenizer = new StringTokenizer(content, " ");
                        int i = 0;
                        while (stringTokenizer.hasMoreTokens()) {
                            String lemma = stringTokenizer.nextToken();
                            if (lemms.containsKey(lemma))
                                lemms.put(lemma, lemms.get(lemma) + 1);
                            i++;
                        }
                        int finalI = i;
                        lemms.forEach((k, v) -> lemms.put(k, v / finalI));
                        resultTF.put(Integer.parseInt(x.getFileName().toString().substring(0, x.getFileName().toString().indexOf("."))), new TreeMap<>(lemms));
                        lemms.forEach((k, v) -> lemms.put(k, 0d));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        List<Double> idf = new ArrayList<>();
        Files.list(Paths.get("/Users/hajrullinbulat/Desktop/учеба/info-search/sitecrawler/result/index"))
                .filter(Files::isRegularFile)
                .sorted()
                .forEach(x -> {
                    try {
                        String content = new String(Files.readAllBytes(x));
                        StringTokenizer stringTokenizer = new StringTokenizer(content, " ");
                        int i = 0;
                        while (stringTokenizer.hasMoreTokens()) {
                            i++;
                            stringTokenizer.nextToken();
                        }
                        idf.add(Math.log(filesCount / i)/Math.log(2));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
        resultTF.forEach((fk, fv) -> {
            final int[] i = {0};
            fv.forEach((sk, sv) ->{
                fv.put(sk, sv * idf.get(i[0]));
                i[0]++;
            });
        });
        StringBuilder tfCol = new StringBuilder();
        final StringJoiner[] tfRow = new StringJoiner[1];
        resultTF.forEach((fk, fv) -> {
            tfRow[0] = new StringJoiner(" ");
            fv.forEach((sk, sv) -> tfRow[0].add(sv.toString()));
            tfCol.append(tfRow[0]);
            tfCol.append("\n");
        });
        FileUtils.writeStringToFile(new File("./result/tf_idf/tfidf_result.txt"), tfCol.toString(), Charset.defaultCharset());
    }
}

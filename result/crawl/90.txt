Pi Day 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 For National Pie Day, see  American Pie Council §  National Pie Day . 
 Pi Day 
 Larry Shaw , the organizer of the first Pi Day celebration at the  Exploratorium  in  San Francisco , note he did not calculate Pi; he just organised a day. 
 Significance 3, 1, and 4 are the three most  significant figures  of  π Celebrations Pie  eating, discussions about  π [1] Date 14 March Next  time March 14, 2018  ( 2018-03 ) Frequency Annual Related  to Pi Approximation Day 
 Part of  a series of articles  on the mathematical constant  π 3.14159 26535 89793 23846 26433... Uses 
 Area of a circle Circumference Use in other formulae 
 Properties 
 Irrationality Transcendence 
 Value 
 Less than 22/7 Approximations Memorization 
 People 
 Archimedes Liu Hui Zu Chongzhi Aryabhata Madhava Ludolph van Ceulen Seki Takakazu Takebe Kenko William Jones John Machin William Shanks John Wrench Chudnovsky brothers Yasumasa Kanada 
 History 
 Chronology Book 
 In culture 
 Legislation Pi Day 
 Related topics 
 Squaring the circle Basel problem Six nines in  π Other topics related to  π 
 
 
 v t e 
 
 
 Pi Day  is an annual celebration of the  mathematical constant   π  (pi) . Pi Day is observed on  14 March  (3/14 in the American  month/day   date format ) since 3, 1, and 4 are the first three  significant digits  of  π . [2] [3]  In 2009, the  United States House of Representatives  supported the designation of Pi Day. [4] 
 Pi Approximation Day  is observed on 22 July (22/7 in the  day/month  date format), since the  fraction  ​ 22 ⁄ 7  is a common  approximation of  π , which is accurate to two decimal places and dates from  Archimedes . [5] 
 
 
 
 Contents 
 
 1   History 2   Observance 3   See also 4   References 5   External links 
 
 
 History [ edit ] 
 The earliest known official or large-scale celebration of Pi Day was organized by  Larry Shaw  in 1988 at the  San Francisco Exploratorium , [6]  where Shaw worked as a  physicist , [7]  with staff and public marching around one of its circular spaces, then consuming fruit pies. [8]  The Exploratorium continues to hold Pi Day celebrations. [9] 
 On March 12, 2009, the U.S. House of Representatives passed a  non-binding resolution  ( 111 H. Res. 224 ), [4]  recognizing March 14, 2009 as National Pi Day. [10]  For Pi Day 2010, Google presented a  Google Doodle  celebrating the holiday, with the word Google laid over images of circles and pi symbols. [11] 
 The entire month of March 2014 (3/14) was observed by some as "Pi Month". [12] [13]  In the year 2015, Pi Day had special significance on 3/14/15 ( mm/dd/yy  date format) at 9:26:53 a.m. and also at p.m., with the date and time representing the first 10 digits of  π . [14]  Pi Day of 2016 was also significant because its  mm/dd/yy  represents pi rounded to the first five digits. 
 Observance [ edit ] 
 Pi Day has been observed in many ways, including eating  pie ,  throwing pies  and discussing the significance of the number  π , due to a  pun  based on the words "pi" and "pie" being homophones in English (  / p aɪ / ), and the coincidental circular nature of a pie. [1] [15] 
 Massachusetts Institute of Technology  has often mailed its  application decision letters  to prospective students for delivery on Pi Day. [16]  Starting in 2012, MIT has announced it will post those decisions (privately) online on Pi Day at exactly 6:28  pm, which they have called "Tau Time", to honor the rival numbers pi and  tau  equally. [17] [18]  In 2015, the regular decisions were put online at 9:26 AM, following that year's "pi moment". [19] 
 Princeton, New Jersey , hosts numerous events in a combined celebration of Pi Day and  Albert Einstein 's birthday, which is also March 14. [20]  Einstein lived in Princeton for more than twenty years while working at the  Institute for Advanced Study . In addition to pie eating and recitation contests, there is an annual Einstein look-alike contest. [20] 
 
 
 
 
 
 
 Pi Pie at  Delft University 
 
 
 
 
 
 
 
 
 Blackberry Pi Pie 
 
 
 
 See also [ edit ] 
 Mole Day Square Root Day Sequential time 
 References [ edit ] 
 
 ^  a   b   Landau, Elizabeth (March 12, 2010).  "On Pi Day, one number 'reeks of mystery ' " . CNN . Retrieved  2018-03-14 .   ^   Bellos, Alex (March 14, 2015).  "Pi Day 2015: a sweet treat for maths fans" .  theguardian.com . Retrieved  March 14,  2016 .   ^   Program on Sveriges Radio – Swedish national radio company  Read March 14, 2015 ^  a   b   United States. Cong. House. Supporting the designation of Pi Day, and for other purposes. 111th Cong.  Library of Congress . ^   "Pi Approximation Day is celebrated today" .  Today In History . Verizon Foundation . Retrieved  January 30,  2011 .   ^   Berton, Justin (March 11, 2009).  "Any way you slice it, pi's transcendental" .  San Francisco Chronicle . Retrieved  March 18,  2011 .   ^   Borwein, Jonathan  (March 10, 2011).  "The infinite appeal of pi" .  Australian Broadcasting Corporation . Retrieved  March 13,  2011 .   ^   Apollo, Adrian (March 10, 2007).  "A place where learning pi is a piece of cake"   (PDF) .  The Fresno Bee .   ^   "Exploratorium 22nd Annual Pi Day" . Exploratorium . Retrieved  January 31,  2011 .   ^   McCullagh, Declan (March 11, 2009).  "National Pi Day? Congress makes it official" .  Politics and Law .  CNET News . Retrieved  March 14,  2009 .   ^   "Pi Day" .  Google Doodles . Google . Retrieved  October 9,  2012 .   ^   Main, Douglas (March 14, 2014).  "It's Not Just Pi Day, It's Pi Month!" . Popular Science . Retrieved  July 22,  2014 .   ^   "Pi Month Celebration  & Circle of Discovery Award Presentation | College of Computer, Mathematical, and Natural Sciences" . Cmns.umd.edu. March 11, 2014 . Retrieved  July 22,  2014 .   ^   Ro, Sam (March 13, 2014).  "March 14, 2015 Will Be A Once-In-A-Century Thrill For Math Geeks" .  Business Insider . Retrieved  March 13,  2014 .   ^   https://www.forbes.com/sites/kionasmith/2018/03/14/wednesdays-google-doodle-celebrates-pi-day/ ^   McClan, Erin (March 14, 2007).  "Pi fans meet March 14 (3.14, get it?)" .  msnbc.com . Retrieved  January 24,  2008 .   ^   "I have SMASHING news!" . MIT Admissions . Retrieved  March 12,  2012 .   ^   McGann, Matt.  "Pi Day, Tau Time" . MIT Admissions . Retrieved  March 18,  2012 .   ^   "Keep your eyes to the skies this Pi Day" . MIT Admissions . Retrieved  2018-03-14 .   ^  a   b   "Princeton Pi Day  & Einstein Birthday Party" .  Princeton Regional Convention and Visitors Bureau . Retrieved  March 14,  2013 .   
 
 External links [ edit ] 
 Wikimedia Commons has media related to  Pi Day . 
 Exploratorium's Pi Day Web Site NPR provides a "Pi Rap" audiovideo Pi Day Professor Lesser's Pi Day page Pi Day in France 
 
 
 
 v t e 
 
 Events commemorating achievements in the sciences 
 Anniversary celebrations 
 
 Alfred Russel Wallace centenary Darwin Centennial Celebration (1959) 
 
 Regular holidays 
 
 Ada Lovelace Day Darwin Day DNA Day Evolution Day Mole Day Pi Day Yuri's Night 
 
 Year long events 
 
 First International Polar Year (1882-1883) Second International Polar Year (1932-1933) International Polar Year (2007-2008) International Geophysical Year International Year of Planet Earth International Year of Astronomy International Year of Chemistry International Year of Crystallography International Year of Light International Space Year World Year of Physics 2005 
 
 
 
 
 
 
 v t e 
 
  Holidays, observances, and celebrations in the United States 
 January 
 
 
 New Year's Day  (federal) Martin Luther King Jr. Day  (federal) 
 
 Confederate Heroes Day  (TX) Fred Korematsu Day  (CA, FL, HI, VA) Idaho Human Rights Day  (ID) Inauguration Day  (federal quadrennial, DC area) Kansas Day  (KS) Lee–Jackson Day  (formerly  Lee–Jackson–King Day ) (VA) Robert E. Lee Day  (FL) Stephen Foster Memorial Day  (36) The Eighth  (LA, former federal) 
 
 January–February 
 
 Super Bowl Sunday 
 
 February 
 American Heart Month 
 Black History Month 
 
 
 Washington's Birthday/Presidents' Day  (federal) Valentine's Day 
 
 Georgia Day  (GA) Groundhog Day Lincoln's Birthday  (CA, CT, IL, IN, MO, NJ, NY, WV) National Girls and Women in Sports Day National Freedom Day  (36) Primary Election Day (WI) Ronald Reagan Day  (CA) Rosa Parks Day  (CA, MO) Susan B. Anthony Day  (CA, FL, NY, WI, WV, proposed federal) 
 
 February–March 
 
 
 Mardi Gras 
 
 Ash Wednesday  (religious) Courir de Mardi Gras  (religious) Super Tuesday 
 
 March 
 Irish-American Heritage Month 
 National Colon Cancer Awareness Month 
 Women's History Month 
 
 
 St. Patrick's Day  (religious) Spring break  (week) 
 
 Casimir Pulaski Day  (IL) Cesar Chavez Day  (CA, CO, TX, proposed federal) Evacuation Day  (Suffolk County, MA) Harriet Tubman Day  (NY) Holi  (NY, religious) Mardi Gras  (AL (in two counties), LA) Maryland Day  (MD) National Poison Prevention Week  (week) Prince Jonah Kūhiō Kalanianaʻole Day  (HI) Saint Joseph's Day  (religious) Seward's Day  (AK) Texas Independence Day  (TX) Town Meeting Day  (VT) 
 
 March–April 
 
 
 Easter  (religious) 
 
 Palm Sunday  (religious) Passover  (religious) Good Friday  (CT, NC, PR, religious) Easter Monday  (religious) 
 
 April 
 Confederate History Month 
 
 420 Day April Fools' Day Arbor Day Confederate Memorial Day  (AL, MS) Days of Remembrance of the Victims of the Holocaust  (week) Earth Day Emancipation Day  (DC) Thomas Jefferson's Birthday  (AL) Pascua Florida  (FL) Patriots' Day  (MA, ME) San Jacinto Day  (TX) Siblings Day Walpurgis Night  (religious) 
 
 May 
 Asian Pacific American Heritage Month 
 Jewish American Heritage Month 
 
 
 Memorial Day  (federal) Mother's Day  (36) Cinco de Mayo 
 
 Harvey Milk Day  (CA) Law Day  (36) Loyalty Day  (36) Malcolm X Day  (CA, IL, proposed federal) May Day Military Spouse Day National Day of Prayer  (36) National Defense Transportation Day  (36) National Maritime Day  (36) Peace Officers Memorial Day  (36) Truman Day  (MO) 
 
 June 
 Lesbian, Gay, Bisexual and 
Transgender Pride Month 
 
 
 Father's Day  (36) 
 
 Bunker Hill Day  (Suffolk County, MA) Carolina Day  (SC) Emancipation Day In Texas / Juneteenth  (TX) Flag Day  (36, proposed federal) Helen Keller Day  (PA) Honor America Days  (3 weeks) Jefferson Davis Day  (AL, FL) Kamehameha Day  (HI) Odunde Festival  (Philadelphia, PA) Senior Week  (week) West Virginia Day  (WV) 
 
 July 
 
 
 Independence Day  (federal) 
 
 Lā Hoʻihoʻi Ea  (HI, unofficial) Parents' Day  (36) Pioneer Day  (UT) 
 
 July–August 
 
 Summer vacation 
 
 August 
 
 American Family Day  (AZ) Barack Obama Day  (IL) Bennington Battle Day  (VT) Hawaii Admission Day / Statehood Day  (HI) Lyndon Baines Johnson Day  (TX) National Aviation Day  (36) Service Reduction Day (MD) Victory over Japan Day  (RI, former federal) Women's Equality Day  (36) 
 
 September 
 Prostate Cancer Awareness Month 
 
 
 Labor Day  (federal) 
 
 California Admission Day  (CA) Carl Garner Federal Lands Cleanup Day  (36) Constitution Day  (36) Constitution Week  (week) Defenders Day  (MD) Gold Star Mother's Day  (36) National Grandparents Day  (36) National Payroll Week  (week) Native American Day  (CA, TN, proposed federal) Patriot Day  (36) 
 
 September–October 
 Hispanic Heritage Month 
 
 
 Oktoberfest 
 
 Rosh Hashanah  (religious) Yom Kippur  (religious) 
 
 October 
 Breast Cancer Awareness Month 
 Disability Employment Awareness Month 
 Filipino American History Month 
 LGBT History Month 
 
 
 Columbus Day  (federal) Halloween 
 
 Alaska Day  (AK) Child Health Day  (36) General Pulaski Memorial Day German-American Day Indigenous Peoples' Day  (VT) International Day of Non-Violence Leif Erikson Day  (36) Missouri Day  (MO) National School Lunch Week Native American Day  (SD) Nevada Day  (NV) Sweetest Day White Cane Safety Day  (36) 
 
 October–November 
 
 Diwali  (religious) 
 
 November 
 Native American Indian Heritage Month 
 
 
 Veterans Day  (federal) Thanksgiving  (federal) 
 
 Day after Thanksgiving  (24) Election Day  (CA, DE, HI, KY, MT, NJ, NY, OH, PR, WV, proposed federal) Family Day  (NV) Hanukkah  (religious) Lā Kūʻokoʻa  (HI, unofficial) Native American Heritage Day  (MD, WA) Obama Day  (Perry County, AL) 
 
 December 
 
 
 Christmas  (religious, federal) 
 
 Alabama Day  (AL) Christmas Eve  (KY, NC, SC) Day after Christmas  (KY, NC, SC, TX) Festivus Hanukkah  (religious, week) Indiana Day  (IN) Kwanzaa  (religious, week) National Pearl Harbor Remembrance Day  (36) New Year's Eve Pan American Aviation Day  (36) Rosa Parks Day  (OH, OR) Wright Brothers Day  (36) 
 
 Varies (year round) 
 
 Eid al-Adha  (religious) Eid al-Fitr  (religious) Ramadan  (religious, month) 
 
 
 Legend:
 (federal) = federal holidays, (state) = state holidays, (religious) = religious holidays, (week) = weeklong holidays, (month) = monthlong holidays, (36) =  Title 36 Observances and Ceremonies 
 Bold  indicates major holidays commonly celebrated in the United States, which often represent the major celebrations of the month. 
See also:  Lists of holidays ,  Hallmark holidays , public holidays in the  United States ,  New Jersey ,  New York ,  Puerto Rico  and the  United States Virgin Islands . 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=Pi_Day &oldid=830374667 "					 
				 Categories :  March observances July observances Observances about science Pi Recurring events established in 1988 Unofficial observances 1988 establishments in California Hidden categories:  Use mdy dates from March 2017 Infobox holiday fixed day (2) 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Article Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read Edit View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 In other projects 
			 
								 Wikimedia Commons 
							 
		 
			 
			 Languages 
			 
								 العربية অসমীয়া Asturianu বাংলা Беларуская (тарашкевіца)‎ Български Català Чӑвашла Čeština Cymraeg Dansk Deutsch Eesti Español Esperanto فارسی Français 한국어 Հայերեն Hrvatski Bahasa Indonesia Íslenska Italiano עברית ಕನ್ನಡ ქართული Kiswahili Lietuvių Magyar Македонски മലയാളം Nederlands नेपाली 日本語 Norsk Norsk nynorsk ଓଡ଼ିଆ Polski Português Română Русский Scots සිංහල Simple English Slovenčina Slovenščina Srpskohrvatski / српскохрватски Suomi Svenska தமிழ் తెలుగు ไทย Türkçe Українська Tiếng Việt 文言 粵語 中文 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 14 March 2018, at 12:49. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.688","walltime":"0.813","ppvisitednodes":{"value":3286,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":97376,"limit":2097152},"templateargumentsize":{"value":3268,"limit":2097152},"expansiondepth":{"value":24,"limit":40},"expensivefunctioncount":{"value":4,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":20635,"limit":5000000},"entityaccesscount":{"value":1,"limit":400},"timingprofile":["100.00%  657.430      1 -total"," 32.35%  212.670      1 Template:Reflist"," 22.54%  148.194      1 Template:Infobox_holiday"," 21.11%  138.784     14 Template:Cite_web"," 16.02%  105.348      1 Template:I nfobox","  7.96%   52.328      2 Template:Navbox","  7.83%   51.448      1 Template:Commons_category","  7.02%   46.179      1 Template:US_Holidays","  6.82%   44.844      1 Template:IPAc-en","  6.13%   40.302      1 Template:For"]},"scribunto":{"limitreport-timeusage":{"value":"0.247","limit":"10.000"},"limitreport-memusage":{"value":5067111,"limit":52428800}},"cachereport":{"origin":"mw1227","timestamp":"20180314124930","ttl":86400,"transientcontent":true}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":65,"wgHostname":"mw1256"});});
Johnson South Reef Skirmish 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 Johnson South Reef Skirmish Part of the  Sino-Vietnamese conflicts 1979–90  and  Spratly Islands dispute 
Map of the  Union Banks , where the skirmish occurred 
 Date 14 March 1988 Location Johnson South Reef Result Chinese victory Territorial 
changes Chinese occupation of Johnson South Reef 
 Belligerents   China   Vietnam Commanders and leaders Chen Weiwen Deputy Brigadier 
Tran Duc Thong; 
Le Lenh Son, CO   HQ-605 ; 
Vu Phi Tru   † , CO   HQ-604 ; 
Vu Huy Le, CO   HQ-505 Units involved 502  Nanchong  (Jiangnan class/065) frigate; 
556  Xiangtan  (Jianghu II class/053H1) frigate; 
531  Yingtan  (Jiangdong class/053K) frigate HQ-505  (ex  Quy Nhon  HQ-504) landing craft; 
 HQ-604  armed transport; 
 HQ-605  armed transport Casualties and losses 1 wounded [1] 64 killed [2] [3] 
11 wounded 
9 captured [4] 
2 armed transporters sunk 
1 landing craft destroyed 
 The  Johnson South Reef Skirmish  was an altercation that took place on 14 March 1988 between  Chinese  and  Vietnamese  forces over  Johnson South Reef  in the  Union Banks  region of the  Spratly Islands  in the  South China Sea . 
 
 
 
 Contents 
 
 1   Background 2   Course 
 2.1   China's account 
 2.1.1   PLAN " 314 " documentary 
 2.2   Vietnam's account 2.3   Independent account 
 3   Aftermath 4   See also 5   Bibliography 6   References 
 
 
 Background [ edit ] 
 The 14th  UNESCO   Intergovernmental Oceanographic Commission  (IOC) agreed that China would establish five observation posts for worldwide ocean survey, including one in the Spratly Islands, [5]  and they commissioned China to build an observation post there in March 1987. [5]  The Delegate of the People's Republic of China (PRC) spoke highly of GLOSS ( Global Sea Level Observing System ) during the meeting of the UNESCO IOC in Paris, but he noted what the PRC considered to be a few mistakes in the text of Document IOC/INF-663; for example, " Taiwan " is listed as a "country" in relevant tables contained in the document. [6] [7]  The scientists from the GLOSS did not know that the PRC claims that Taiwan is not a separate country; nor did they know about the territorial disputes in the South China Sea. They agreed that China would install  tide gauges  on its coasts in the East China Sea, and on what the PRC calls its Nansha Islands in the South China Sea. The scientists did not know that Taiwan occupied  one of the Spratly Islands , but (despite its territorial claims), at that time China occupied none. [8]  After numerous surveys and patrols, in April 1987 China chose  Fiery Cross Reef  as the ideal (from their point of view) location for the observation post, because the unoccupied reef was remote from other settlements, and it was large enough for the observation post. [5]  On the other hand,  Johnson South Reef  in the  Union Banks  sunken atoll, (150  km east of  Fiery Cross Reef ), is close to the Vietnamese inhabited  Sin Cowe Island  (also in the Union Banks), and it is also within the Philippine claimed 200 nautical mile  Economic Exclusion Zone ; in other words, Johnson South Reef is in a high profile and highly disputed area. [9] [10]  In January and February 1988, Vietnamese forces began establishing a presence at surrounding reefs, including Collins Reef and Lansdowne Reef in the Union Banks, in order to monitor Chinese activity. [5]  This led to a series of confrontations. [5] 
 Course [ edit ] 
 China's account [ edit ] 
 On 13 March, the frigate  Nanchong  detected  People's Army of Vietnam  (PAVN) armed naval transport  HQ-604  heading toward  Johnson South Reef , transport  HQ-605  heading toward Lansdowne Reef, and landing craft  HQ-505  heading toward  Collins Reef  in a simultaneous three-pronged intrusion upon the disputed reefs. [11] 
 At approximately 07:30 on Johnson South Reef, Vietnamese troops attempted to erect the  Vietnamese flag  on the reef. It was reported that PAVN Corporal Nguyen Van Lanh and PAVN Sub Lieutenant Tran Van Phuong argued over the flag raising with  People's Liberation Army Navy  (PLAN) sailor Du Xianghou, which led to a pitched battle between the opposing forces on the reef. In response, Vietnamese forces, with naval transport  HQ-604  in support, opened fire. [11]  PLAN forces and the frigate  Nanchong  counter-attacked at 08:47 hours. Transport  HQ-604  was set ablaze and sunk. [11] 
 At 09:15 hours, the frigate  Xiangtan  arrived at Lansdowne Reef and found that nine Vietnamese marines from transport  HQ-605  had already landed. The frigate  Xiangtan  immediately hailed the Vietnamese and demanded they withdraw from the reef. Instead, the Vietnamese opened fire. [11]   HQ-605  was damaged heavily and finally sunk by the Chinese. [11] 
 PLAN " 314 " documentary [ edit ] 
 The PLAN filmed the skirmish and produced a propaganda documentary called " 314 " meaning "March 14" in Chinese, which was published on the Internet. After a Chinese landing attempt and withdrawal, the Vietnamese soldiers formed a line on the reef to protect the three Vietnamese flags. The PLAN frigates eventually fire on the Vietnamese on the reef as well as the Vietnamese ships, which were sunk. 
 Vietnam's account [ edit ] 
 In January 1988,  China  sent a group of ships from  Hainan  to the southern part of the  South China Sea . This included four ships, including three frigates, despatched to the north-west of the Spratly Islands. The four ships then began provoking and harassing the Vietnamese ships around  Tizard Bank  and the  London Reefs . Vietnam believed this battle group intended to create a reason to "occupy the Spratly Islands in a preventive counterstrike". [12] 
 In response, two transport ships from the Vietnamese Navy's 125th Naval Transport Brigade,  HQ-604  and  HQ-505 , were mobilized. They carried nearly 100 army officers and men to  Johnson South Reef  (Đá Gạc Ma),  Collins Reef  (Đá Cô Lin), and Lansdowne Reef (Đá Len Đao) in the Spratly Islands. [13]  On 14 March 1988, as the soldiers from  HQ-604  were moving construction materials to  Johnson South Reef , the four Chinese ships arrived. [13]  The three Chinese frigates approached the reef: 
 frigate 502  Nanchong , (Type 65 (Jiangnan class)). Displaces 1,400 tons, equipped with three 100 mm guns and eight 37 mm  AA guns . [14] frigate 556  Xiangtan , (Jianghu II class / 053H1). Displaces 1,925 tons, equipped with four 100 mm guns and two 37 mm AA guns. [15] frigate 531  Yingtan , (Jiangdong class / 053K). Displaces 1,925 tons, equipped with four 100 mm guns and eight 37 mm AA guns. [16] 
 Commander Tran Duc Thong ordered Second Lieutenant Tran Van Phuong and two men, Nguyen Van Tu and Nguyen Van Lanh, to rush to the reef in a small boat and the Vietnamese flag that had been planted there the previous day. [13]  The Chinese landed armed soldiers on the reef, and the PLAN frigates opened fire on the Vietnamese ships. Both the HQ-604 armed transport and HQ-605 armed transport were sunk. [13]  The HQ-505 armed transport was ordered to run aground on Collins reef to prevent the Chinese from taking it. [13] 
 Vietnamese soldiers, most of them unarmed, [4] [17]  formed a circle on the reef to protect the Vietnamese flag. The Chinese attacked, and the Vietnamese soldiers resisted as best they could. [13]  A skirmish ensued in which the Chinese shot and  bayoneted  some Vietnamese soldiers to death, but the Chinese were unable to capture the flag. [13]  The Chinese finally retreated enabling PLAN frigates to open fire on the reef's defenders. When all of the Vietnamese had been killed or wounded, the Chinese occupied the reef and began building a bunker. 64 Vietnamese soldiers had been killed in the battle according to Vietnamese reports. [12] [18]  Vietnam also accused China of refusing to allow Vietnam's  Red Cross  ship to recover bodies and rescue wounded soldiers. [19] 
 Independent account [ edit ] 
 Cheng Tun-jen and Tien Hung-mao, two American professors, summarized the skirmish as follows: in late 1987, the PRC started deploying troops to some of the unoccupied reefs of the Spratly Islands. Soon after the PLA stormed the Johnson South Reef on 14 March 1988, a skirmish began between Vietnamese troops and PRC landing parties. Within a year, the PLA occupied and took over s even reefs and rocks in the Spratly Islands. [20] 
 Koo Min Gyo, Assistant Professor in the Department of Public Administration at Yonsei University, Seoul, South Korea, reported the battle's course was as follows: On 31 January 1988, two Vietnamese  armed cargo ships  approached the Fiery Cross Reef to get construction material to build structures signifying Vietnam's claim over the reef. [5]  However, the PLAN intercepted the ships and forced them away from the reef. [5]  On 17 February, a group of Chinese ships (a PLAN destroyer, escort, and transport ships) and several Vietnamese ships (a  minesweeper  and armed freighter) all attempted to land troops at  Cuarteron Reef . Eventually the outgunned Vietnamese ships were forced to withdraw. [5]  On 13 and 14 March, a PLAN artillery frigate was surveying the Johnson Reef when it spotted three Vietnamese ships approaching its location. [5]  Both sides dispatched troops to occupy Johnson Reef. [5]  After shots were fired by ground forces on the reef, the Chinese and Vietnamese ships opened fire on each other. [5] 
 Aftermath [ edit ] 
 China moved quickly to consolidate its presence. By the end of 1988, it had occupied six reefs and atolls in the Spratly Islands. [5] 
 On 2 September 1991, China released the nine Johnson South Reef Skirmish Vietnamese prisoners. [4] 
 In 1994, China had a similar confrontation by asserting its ownership of  Mischief Reef , which was inside the claimed  EEZ  of the  Philippines . However, the Philippines only made a political protest, since according to the  Henry L. Stimson Center , the  Philippine Navy  decided to avoid direct confrontation. This was partly based on the Johnson South Reef Skirmish, in which the Chinese had killed Vietnamese troops even though the conflict took place near the Vietnamese-controlled area. [21] 
 See also [ edit ] 
 Spratly Islands dispute Battle of the Paracel Islands Naval history of China 
 Bibliography [ edit ] 
 The South China Sea Online Resource Kelly, Todd C. (1999). "Vietnamese Claims to the Truong Sa Archipelago". Explorations in Southeast Asian Studies Vol 3 
 References [ edit ] 
 
 
 ^   海南省地方志编纂委员会 (1993-11-22). "第三章第四节 自卫反击战".  海南省省志  (in Chinese). 北京: 方志出版社.  ISBN   9787514412376 .  南沙群岛历来是中国领土。l987年，应联合国教科文组织的要求，中国政府决定在南沙群岛永署礁建立1座海洋观测站。1988 年3月13日，我海军奉命组织舰船和人员在南沙群岛的赤瓜礁等岛礁进行考查。14日，越军海军604、605号运输船和505号登陆舰悍然窜到赤瓜礁海区进行挑畔活动，并派出43名海军人员强行登上赤瓜礁。越军无视中国考察人员的警告，首先使用冲锋枪向我岛上人员开火，打伤我考察人员1人。接着，越南海军舰船又向停泊在赤瓜礁附近海区的我海军舰船开枪。我海军考察舰船的人员，为捍卫祖国领海、领土主权，在忍无可忍的情况下，被迫进行自卫还击，击沉越南海军604号运输船，击伤605号运输船和重创505号登陆舰，胜利地保卫了祖国的领海、领土。   ^   Martin Petty; Simon Cameron-Moore.  "Vietnam protesters denounce China on anniversary of navy battle" . Reuters.   ^   TRƯỜNG TRUNG - QUỐC NAM.  "Lễ tưởng niệm 64 anh hùng liệt sĩ bảo vệ Gạc Ma" .  Tuổi Trẻ .   ^  a   b   c   "Deadly fight against Chinese for Gac Ma Reef remembered" . Thanh Nien News. 14 March 2013.   ^  a   b   c   d   e   f   g   h   i   j   k   l   Koo, Min Gyo (2009).  Island Disputes and Maritime Regime Building in East Asia . Dordrecht: Springer. p.  154.  ISBN   978-1-4419-6223-2 .   ^   Taiwan's official title is in fact the " Republic of China ", though within Taiwan and internationally the nation is commonly referred to as "Taiwan, Republic of China", or simply "Taiwan". ^   "IOC. Assembly; 14th session; (Report)"   (PDF) . 1 April 1987. p.  41.   ^   "South China Sea Treacherous Shoals",  Far Eastern Economic Review , 13 August 1992: p14-17 ^   "Territorial claims in the Spratly and Paracel Islands" . GlobalSecurity.org. 11 July 2011 . Retrieved  4 June  2014 .   ^   "Digital Gazetteer of Spratly Islands" . www.southchinasea.org. Archived from  the original  on 2007-07-17 . Retrieved  2008-02-08 .   
- Version dated 19 August 2011 is available at:  "Digital Gazetteer of Spratly Islands" . www.southchinasea.org. 19 August 2011 . Retrieved  5 June  2014 .  This list includes the names of all Spratly features known to be occupied and/or above water at low tide.   ^  a   b   c   d   e   " Secrets of the Sino-Vietnamese skirmish in the South China Sea ", WENWEIPO.COM LIMITED, March 14, 1988. ^  a   b   Hồng Chuyên.  "Một phần Trường Sa của Việt Nam bị Trung Quốc chiếm như thế nào? (bài 8) (How China took a part of Vietnam's Spratly Islands)" .  infornet . Infornews . Retrieved  19 March  2014 .   ^  a   b   c   d   e   f   g   QUỐC VIỆT (1988-03-14).  " " Vòng tròn bất tử" trên bãi Gạc Ma ( The immortal circle in the Johnson South Reef )" .  Tuổi Trẻ . Retrieved  19 March  2014 .   ^   "Jiangnan - People's Liberation Army Navy" .  fas.org . Retrieved  31 July  2016 .   ^   "Jianghu-class frigates - People's Liberation Army Navy" .  fas.org . Retrieved  31 July  2016 .   ^   "Jiangdong-class Frigate - People's Liberation Army Navy" .  fas.org . Retrieved  31 July  2016 .   ^   Mai Thanh Hai - Vu Ngoc Khanh (14 March 2016).  "Vietnamese soldiers remember 1988 Spratlys battle against Chinese" .  thanhniennews.com .  Thanh Nien News . Retrieved  19 March  2014 .   ^   H.QUÂN - V.TÌNH - X.HOÀI (2014-03-14).  "Tưởng niệm 64 anh hùng liệt sĩ hy sinh bảo vệ đảo Gạc Ma ngày 14-3-1988 ( Honoring 64 martyrs who died for protecting the Johnson South Reef in 14-03-1988 )" . Vietbao . Retrieved  19 March  2014 .   ^   Từ Đặng Minh Thu (7 January 2008).  "Tranh chấp Trường Sa - Hoàng Sa: Giải quyết cách nào? ( Spratly Islands and Paracel Islands dispute: How to resolve? )" .  Công an Thành Phố Hồ Chí Minh . Công an Thành Phố Hồ Chí Minh Magazine . Retrieved  19 March  2014 .   ^   Cheng, Tun-jen; Tien, Hung-mao (2000).  The Security environment in the Asia-Pacific . Armonk, N.Y: M.E. Sharpe. p.  264.  ISBN   0-7656-0539-2 .   ^   Cronin, Richard P. (2010-02-04).  "China's Activities in Southeast Asia and the Implications for U.S. Interests"   (PDF) . www.uscc.gov.   
 
 
 
 
 
 v t e 
 
 Armed conflicts  involving the  People's Republic of China 
 Internal 
 
 Chinese Civil War  (1927–50) Kuomintang Islamic insurgency  (1950–58) Kuomintang insurgency  (1949-1961) Battle of Chamdo  (1950) Tibetan uprising  (1959) Xinjiang conflict  (1980–present) Tiananmen Square protests  (1989) 
 
 Cross-Taiwan Strait 
(vs  Taiwan ) 
 (after 1 Oct 1949) 
 
 Kuningtou  (1949) Dengbu Island  (1949) Hainan Island  (1950) Nanri Island  (1952) Dongshan Island  (1953) Yijiangshan Islands  (1955) Dachen Archipelago  (1955) Second Taiwan Strait Crisis  (1958) Burmese border Dong-Yin  (1965) Third Taiwan Strait Crisis  (1995–96) 
 
 Others 
 
 vs  USSR 
 
 Sino-Soviet border conflict  (1969) 
 
 vs  United States  and allies 
 
 Korean War  (1950–53) Vietnam War  (1965–70) 
 
 vs  India 
 
 Sino-Indian War  (1962) Chola incident  (1967) Sino-Indian skirmish  (1987) Sino-Indian Standoff  (2017) 
 
 vs  South Vietnam / Vietnam 
 
 Paracel Islands  (1974) Sino-Vietnamese War  (1979) Sino-Vietnamese conflicts 1979–90 Johnson South Reef Skirmish  (1988) 
 
 
 See also 
 
 Incorporation of Tibet into the People's Republic of China Incorporation of Xinjiang into the People's Republic of China 
 
 
 
 
 
 
 v t e 
 
 Spratly Islands 
 Related articles 
 
 Spratly Islands dispute Great wall of sand History of the Spratly Islands List of airports in the Spratly Islands List of maritime features in the Spratly Islands Philippines and the Spratly Islands Republic of Morac-Songhrati-Meads Territorial disputes in the South China Sea Nine-dash line 
 
 Confrontations 
 
 Southwest Cay incident (1975) Johnson South Reef Skirmish  (1988) 
 
 Regions 
 
 Dangerous Ground Loaita Bank London Reefs North Danger Reef Reed Bank Tizard Bank Union Banks 
 
 Occupied features 
 
   China 
 
 Cuarteron Reef Fiery Cross Reef Gaven Reefs Hughes Reef Johnson South Reef Mischief Reef Subi Reef 
 
   Malaysia 
 
 Ardasier Reef Dallas Reef Erica Reef Investigator Shoal Mariveles Reef Swallow Reef 
 
   Philippines 
 
 Commodore Reef Flat Island Irving Reef Lankiam Cay Loaita Island Nanshan Island Northeast Cay Second Thomas Shoal Thitu Island West York Island 
 
   Taiwan 
 
 Taiping Island Zhongzhou Reef 
 
   Vietnam 
 
 Amboyna Cay Bombay Castle Collins Reef Cornwallis South Reef Ladd Reef Namyit Island Sand Cay Sin Cowe Island Southwest Cay Spratly Island 
 
 
 Unoccupied features 
 
 Half Moon Shoal Louisa Reef Luconia Shoals North East Investigator Shoal Royal Captain Shoal Sabina Shoal Western Reef 
 
 
 
 
 
 
 v t e 
 
 South China Sea 
 Pratas Islands 
 
 Pratas Island 
 
 Paracel Islands 
 
 Amphitrite Group 
 
 Rocky Island Tree Island West Sand Woody Island Qilian Yu 
 
 Crescent Group 
 
 Money Island Robert Island Yagong Island 
 
 Other features 
 
 Bombay Reef Triton Island 
 
 
 NorthEast SCS 
 
 Zhongsha Islands Macclesfield Bank 
 Walker Shoal 
 Scarborough Shoal 
 
 Spratly Islands 
 
 List of maritime features in the Spratly Islands Great Wall of Sand Royal Malaysian Navy Offshore Bases Vietnamese DK1 rigs List of airports in the Spratly Islands 
 
 Dangerous 
Ground 
 
 NW 
 
 North Danger Reef 
 Northeast Cay Southwest Cay 
 Thitu Reefs
 Thitu Island Subi Reef 
 Loaita Bank 
 Lankiam Cay Loaita Island 
 Tizard Bank 
 Ban Than Reef Gaven Reefs Itu Aba Namyit Island Sand Cay 
 NNW
 Irving Reef West York Island 
 WNW
 Western Reef 
 
 
 NE 
 
 Flat Island Nanshan Island Reed Bank Third Thomas Shoal 
 
 SE 
 
 Commodore Reef First Thomas Shoal Mischief Reef Sabina Shoal Second Thomas Shoal 
 
 SW 
 
 Union Banks 
 Collins Reef Hughes Reef Johnson South Reef Sin Cowe Island 
 Ardasier Reef Cornwallis South Reef Dallas Reef Erica Reef Investigator Shoal Mariveles Reef 
 
 
 West 
 
 London Reefs 
 Central London Reef Cuarteron Reef East London Reef West London Reef 
 Bombay Castle Fiery Cross Reef Ladd Reef Spratly Island 
 
 East 
 
 Royal Captain Shoal Half Moon Shoal 
 
 South 
 
 Amboyna Cay Louisa Reef Swallow Reef 
 
 
 Southern SCS 
 
 James Shoal Luconia Shoals 
 
 Tudjuh Archipelago 
 
 Natuna Islands Anambas Islands Badas Islands Tambelan Archipelago 
 
 History 
 
 Territorial disputes History of the Spratly Islands Nine-Dash Line Spratly Islands dispute Philippines and the Spratly Islands Battle of the Paracel Islands  (1974) Southwest Cay incident  (1975) Johnson South Reef Skirmish  (1988) Scarborough Shoal standoff  (2012) Hai Yang Shi You 981 standoff  (2014) 
 
 Transport 
 
 Ships
 Coconut Princess 
 Airports
 Pratas Is Paracel Islands Airports
 Woody Is 
 Spratly Islands Airports 
 Itu Aba Spratly Is Swallow Reef Thitu Is 
 
 
 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=Johnson_South_Reef_Skirmish &oldid=830372899 "					 
				 Categories :  Indochina Wars Conflicts in 1988 1988 in China 1988 in Vietnam Military history of Vietnam Military history of the People's Republic of China Naval battles involving Vietnam Naval battles involving China History of the Spratly Islands History of the South China Sea China–Vietnam military relations March 1988 events Hidden categories:  CS1 Chinese-language sources (zh) 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Article Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read Edit View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 Languages 
			 
								 Deutsch 한국어 Bahasa Indonesia Italiano 日本語 Norsk Tiếng Việt 中文 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 14 March 2018, at 12:34. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.392","walltime":"0.445","ppvisitednodes":{"value":1790,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":123959,"limit":2097152},"templateargumentsize":{"value":739,"limit":2097152},"expansiondepth":{"value":12,"limit":40},"expensivefunctioncount":{"value":0,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":27853,"limit":5000000},"entityaccesscount":{"value":0,"limit":400},"timingprofile":["100.00%  334.628      1 -total"," 56.74%  189.881      1 Template:Reflist"," 26.32%   88.058      8 Template:Navbox"," 26.01%   87.041     14 Template:Cite_web"," 15.60%   52.212      3 Template:Cite_book" ," 14.76%   49.393      1 Template:Infobox_military_conflict","  8.81%   29.481      1 Template:Spratly_Islands_topics","  5.96%   19.935      5 Template:Flagicon","  4.81%   16.103      1 Template:PRC_conflicts","  4.54%   15.183      1 Template:South_China_Sea"]},"scribunto":{"limitreport-timeusage":{"value":"0.163","limit":"10.000"},"limitreport-memusage":{"value":3662743,"limit":52428800}},"cachereport":{"origin":"mw1231","timestamp":"20180314123440","ttl":1900800,"transientcontent":false}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":69,"wgHostname":"mw1246"});});
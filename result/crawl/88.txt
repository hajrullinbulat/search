2018 Winter Paralympics 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 See also:  2018 Winter Olympics 
 XII Winter Paralympics 
 Pyeongchang 2018 Paralympics official logo 
 Host city Pyeongchang ,  South Korea Motto Passion. Connected. 
 Korean : 하나된 열정. Nations participating 49 Athletes participating Over 570 [1] Events 80 in 6 sports  [2] Opening ceremony 9 March Closing ceremony 18 March Officially opened by President   Moon Jae-in Paralympic torch Kim Eun-jung 
 Seo Soon-seok Paralympic stadium Pyeongchang Olympic Stadium Winter 
 <    Sochi 2014 Beijing 2022     > 
 Summer 
 <    Rio 2016 Tokyo 2020     > 
 
 Part of a series on 
 
 2018 Winter Paralympics 
 
 Bid process Olympics Event calendar Venues Mascots Concerns and controversies Torch Relay  ( route ) Medal table  ( medalists ) Opening ceremony  ( flag bearers ) Chronological summary Closing ceremony 
 
 IPC KPC POCOG 
 
 
 v t e 
 
 
 
 
 The  2018 Winter Paralympics  ( Hangul :   평창 동계 패럴림픽 ;  Hanja :   平昌 冬季 패럴림픽 ;  RR :   Pyeongchang Donggye Paereollimpik ), the 12th  Paralympic Winter Games , and also more generally known as the  PyeongChang 2018 Paralympic Winter Games , are an international  multi-sport event  for  athletes with disabilities  governed by the  International Paralympic Committee  (IPC), that is currently taking place in  Pyeongchang County ,  Gangwon Province ,  South Korea , from 9 to 18 March 2018. 
 Athletes representing 49  National Paralympic Committees  are participating in these Games, which mark the first time that South Korea has hosted the Winter Paralympics and the second Paralympics held in the country overall, after the  1988 Summer Paralympics  in Seoul. 
 
 
 
 Contents 
 
 1   Host selection 2   Opening ceremony 3   Sports 4   Calendar 5   Participating National Paralympic Committees 6   Venues 
 6.1   Pyeongchang Mountain cluster 
 6.1.1   Alpensia 6.1.2   Standalone venue 
 6.2   Gangneung coastal cluster 
 7   Medals 8   Broadcasting 9   Tickets 10   Marketing 
 10.1   Emblem 10.2   Mascot 10.3   Cultural events 
 11   Concerns and controversies 
 11.1   North Korean relations 
 12   See also 13   Notes 14   External links 
 
 
 Host selection [ edit ] 
 Main article:  Bids for the 2018 Winter Olympics 
 As part of a formal agreement between the  International Paralympic Committee  and the  International Olympic Committee  first established in 2001, [3]  the winner of the bid for the  2018 Winter Olympics  was also to host the 2018 Winter Paralympics. 
 Pyeongchang was elected as host during the  123rd IOC Session  in  Durban  in 2011, earning the required majority of at least 48 votes in just one round of voting. [4] [5] 
 2018 Winter Olympics bidding results City Nation Votes Pyeongchang   South Korea 63 Munich   Germany 25 Annecy   France 7 
 Opening ceremony [ edit ] 
 
 
 
 
 Pyeongchang Olympic Stadium  in March 9, 2018 
 
 
 Main article:  2018 Winter Paralympics opening ceremony 
 The opening ceremony was held on 9 March 2018. The site of the opening ceremony  Pyeongchang Olympic Stadium  was built specifically for the games. [6] 
 Sports [ edit ] 
 Competitions in the 2018 Winter Paralympics are being held in six  Winter Paralympic sports , with 80 medal events in total.  Snowboarding  has been expanded into a separate discipline for 2018, with 10 medal events (in 2014, two medal events in snowboarding were held within the alpine skiing programme). 
   Alpine skiing   (30)  ( details )   Biathlon   (18)  ( details )   Cross-country skiing   (20)  ( details )   Para ice hockey   (1)  ( details )   Snowboarding   (10)  ( details )   Wheelchair curling   (1)  ( details ) 
 Calendar [ edit ] 
 In the following calendar for the 2018 Winter Paralympics, each blue box represents an event competition. The yellow boxes represent days during which medal-awarding finals for a sport are held. The number in each yellow box represents the number of finals that are contested on that day. [7] 
  ●   Opening ceremony         Event competitions         Event finals  ●   Closing ceremony 
 March  Fri 
9th Sat 
10th Sun 
11th Mon 
12th Tue 
13th Wed 
14th Thu 
15th Fri 
16th Sat 
17th Sun 
18th Gold 
Medals Ceremonies OC CC N/A Alpine skiing 6 6 6 6 6 30 Biathlon 6 6 6 18 Cross-country skiing 2 4 6 6 2 20 Para ice hockey ● ● ● ● ● ● ● ● 1 1 Snowboarding 5 5 10 Wheelchair curling ● ● ● ● ● ● ● 1 1 Total 0 12 8 9 12 12 6 11 7 3 80 
 Participating National Paralympic Committees [ edit ] 
 Participating  National Paralympic Committees 
 
   Andorra   (1)   Argentina   (2)   Armenia   (1)   Australia   (12) [8]   Austria   (13)   Belarus   (14)   Belgium   (2)   Bosnia and Herzegovina   (1)   Brazil   (3) [9]   Bulgaria   (1)   Canada   (52) [10]   Chile   (4)   China   (26) [11]   Croatia   (7) [12]   Czech Republic   (21)   Denmark   (1) [13]   Finland   (13)   France   (12) [14]   Georgia   (2)   Germany   (20) [15]   Great Britain   (14) [16]   Greece   (1)   Hungary   (2)   Iceland   (1)   Iran   (5)   Italy   (26)   Japan   (38)   Kazakhstan   (6)   North Korea   (2) [17]   South Korea   (36)   (host)   Mexico   (1) [18]   Mongolia   (1)   Netherlands   (9)   Neutral Paralympic Athletes   (30)   New Zealand   (3)   Norway   (32)   Poland   (9)   Romania   (1)   Serbia   (1)   Slovakia   (11)   Slovenia   (1)   Spain   (3)   Sweden   (24) [19]   Switzerland   (13)   Tajikistan   (1)   Turkey   (1)   Ukraine   (20)   United States   (68)   Uzbekistan   (1) 
 
 
 Russia is currently suspended by the  International Paralympic Committee  due to the  state-sponsored doping program scandal . [20]  However, the IPC has allowed Russian athletes to qualify as  neutral participants . They will field around 30–35 athletes in 5 sports, participating as  Neutral Paralympic Athletes  (NPA), and will also march under the Paralympic flag at the Opening and Closing Ceremonies, and the Paralympic anthem will be played in any ceremony. [21] Three nations will make their Winter Paralympic debut: North Korea, Georgia and Tajikistan. [ citation needed ] 
 Venues [ edit ] 
 Main article:  Venues of the 2018 Winter Olympics and Paralympics 
 Pyeongchang Mountain cluster [ edit ] 
 Alpensia [ edit ] 
 The Alpensia Resort in  Daegwallyeong-myeon  will be the focus of the 2018 Pyeongchang Winter Paralympics. [22] 
 Alpensia Resort  – Biathlon, cross country skiing Main Olympic Village – Athletes Village Pyeongchang Olympic Stadium  – Awards and Opening and Closing Ceremonies 
 Standalone venue [ edit ] 
 Jeongseon Alpine Centre  – alpine skiing and snowboarding  [23] 
 Gangneung coastal cluster [ edit ] 
 Gangneung Curling Centre  – Wheelchair Curling Gangneung Hockey Centre  – Para Ice Hockey 
 Medals [ edit ] 
 Main article:  2018 Winter Paralympics medal table 
          Host nation (South Korea) [24] 
 Rank NPC Gold Silver Bronze Total 1   United States   (USA) 8 7 6 21 2   Neutral Paralympic Athletes   (NPA) 6 7 4 17 3   Slovakia   (SVK) 6 2 1 9 4   France   (FRA) 5 5 3 13 5   Ukraine   (UKR) 5 4 7 16 6   Canada   (CAN) 5 1 10 16 7   Germany   (GER) 4 7 1 12 8   Belarus   (BLR) 3 4 2 9 9   Switzerland   (SUI) 3 0 0 3 10   Netherlands   (NED) 2 3 0 5 11–22 Remaining NPCs 6 13 20 39 Total 53 53 54 160 
 Broadcasting [ edit ] 
 Television rights were sold in various countries and territories; the IPC partnered with the IOC's streaming service  Olympic Channel  for online streaming coverage of these Paralympics. [25] 
 In 2017, the  European Broadcasting Union  renewed its rights to the Paralympics in 25 European countries through 2020  [26]  In the United States,  NBC Sports  announced plans to air nearly twice as much coverage on linear television as it did in Sochi, with 94 hours airing primarily on  NBCSN  and the U.S. version of  Olympic Channel , along with online streaming content. [27] [28]  In Canada, the  CBC  announced that it would broadcast over 600 hours of coverage in  English  and  French  across its platforms, including  CBC Television ,  Ici Radio-Canada Télé , and sublicence partners  Sportsnet One  and  AMI-tv . [29]   Channel 4  returned as the Games' rightsholder in the United Kingdom, planning over 100 hours of television coverage on Channel 4 and  4Seven . [30] 
 Tickets [ edit ] 
 The ticket prices for the 2018 Winter Paralympics were announced on 8 June 2017 and tickets went on sale on 21 August 2017. [31] 
 Prices for sporting event tickets range from  ₩ 10,000 to 50,000 (approx. $8–45 USD). Opening and closing ceremony tickets range from ₩10,000 to ₩140,000 (approx. $8-125 USD). [31] [32] 
 As of 19 January, tickets to the Paralympic Games were 70% sold. (155,000 tickets out of a total of 223,353 allocated). [33] [34] [35] 
 Marketing [ edit ] 
 Emblem [ edit ] 
 The emblem for the 2018 Winter Paralympics was unveiled on 29 October 2013 at the  National Museum of Korea . It incorporates stylized renditions of the  hangul  letter  ㅊ   ch  (as also used in the Olympic emblem), which symbolizes part of the name  Pyeongchang  and resembles ice crystals. The Paralympic emblem features two of these letters joined together, symbolizing equality and a "grand" festival welcoming international athletes and spectators. [36] [37] 
 Mascot [ edit ] 
 Main article:  Soohorang and Bandabi 
 The official mascots of the 2018 Winter Olympics and Paralympics were unveiled on 2 June 2016. The Paralympic mascot, Bandabi, is an  Asian black bear  that symbolizes "strong will and courage". [38] 
 Cultural events [ edit ] 
 To attract interest from residents and foreign tourists, the  Korea Tourism Organization  organized  Snow Festival , a " Hallyu  festival", to serve as cultural programming for the Paralympics. Actors  Jang Keun-suk  and  Lee Dong-wook  purchased 2,018 and 1,000 tickets for themselves and fans to attend sledge hockey games accompanied by meetups, while a  K-pop  concert featuring  B1A4  and  BtoB  was also organized. [39] [40] [41] [42] [43] 
 Concerns and controversies [ edit ] 
 North Korean relations [ edit ] 
 See also:  North Korea–South Korea relations  and  North Korea at the 2018 Winter Paralympics 
 Prior to the  2018 Winter Olympics ,  North Korea  agreed to have its athletes march together with those of the South Korean team during the opening ceremonies, and field a unified women's hockey team. However, during a meeting in Pyeongchang between the leaders of their National Paralympic Committees, the two countries were unable to organize a similar arrangement for the Paralympics. The South Korean Paralympic Committee stated that North Korean officials had requested that the  Liancourt Rocks  (which are the subject of an  ongoing sovereignty dispute  between South Korea and Japan) be included on the  Korean Unification Flag  during the Paralympics. South Korea declined this request, as they considered it contradictory to IPC recommendations against political gestures. [44] 
 IPC president  Andrew Parsons  expressed disappointment over the decision, but noted that the country "respects and values the IPC's vision and mission" and had "committed to working further with the IPC to improve the lives of people with an impairment in North Korea", while also acknowledging that the IOC had "made great progress in opening up dialogue between the two nations" prior to the Olympics, and that their meeting "underlines the tremendous ability of sport to bring countries together in positive dialogue." [45] 
 See also [ edit ] 
 
 Winter Paralympics portal 
 
 Notes [ edit ] 
 
 
 ^   "About Pyeongchang 2018 Winter Games" . Retrieved  9 February  2018 .   ^   [1] ,  International Paralympic Committee  (IPC) ^   "Paralympics 2012: London to host 'first truly global Games ' " . BBC Sport . Retrieved  1 August  2012 .   ^   Longman, Jeré; Sang-hun, Choe (6 July 2011).  "2018 Winter Games to Be Held in Pyeongchang, South Korea" .  The New York Times .  ISSN   0362-4331 .  Archived  from the original on 4 August 2017 . Retrieved  7 June  2017 .   ^   Hersh, Philip (6 July 2011).  "Pyeongchang wins 2018 Winter Olympics" .  Chicago Tribune .  Archived  from the original on 9 April 2017.   ^   Horwitz, Josh (25 January 2018).  "South Korea's $100 million Winter Olympics stadium will be used exactly four times" .  Quartz . Retrieved  9 March  2018 .   ^   "CoSport – Paralympic Event Tickets" .  www.cosport.com . Retrieved  9 February  2018 .   ^   "Australia's Seven Network to show PyeongChang 2018" . Retrieved  8 January  2018 .   ^   "Brasil terá três atletas na Paralimpíada de Inverno de PyeongChang 2018" . Retrieved  28 February  2018 .   ^   "PyeongChang 2018" . Retrieved  8 January  2018 .   ^   "平昌冬残奥会中国体育代表团在京成立" . 中国中央电视台. 3 March 2018.   ^   Paralympic Games (19 January 2018).  "@Paralympic Twitter account" .  Twitter . Retrieved  19 January  2018 .   ^   "PyeongChang 2018: Daniel Wagner's Winter Games dream" . Retrieved  8 January  2018 .   ^   "PyeongChang 2018: Bochet to carry French flag" . Retrieved  8 January  2018 .   ^   "Germany reveal kit for PyeongChang 2018" . Retrieved  8 January  2018 .   ^   "British alpine skiers and snowboarders named for PyeongChang 2018" . Retrieved  8 January  2018 .   ^   "North Korea invited to participate in first-ever Winter Paralympic Games at Pyeongchang 2018" . 9 February 2018 . Retrieved  9 February  2018 .   ^   "PyeongChang 2018: Arly Velasquez ready to surprise" . Retrieved  8 January  2018 .   ^   Hjelmberg, Henrik.  "Svenska truppen till Paralympics uttagen" . Sveriges Paralympiska Kommitté . Retrieved  25 February  2018 .   ^   "With one year until 2018 Winter Games, Russia's status murky" . 9 February 2017.   ^   "IPC allow Russian athletes to compete as neutrals at Pyeongchang 2018 but maintain suspension" . Retrieved  9 February  2018 .   ^   PyeongChang 2018 Alpensia Resort and water park complete and full for summer season   Archived  12 March 2012 at the  Wayback Machine . ^   "Jeongseon Alpine Centre to host Para alpine skiing and snowboard in 2018" .  www.paralympic.org . Retrieved  9 February  2018 .   ^   "Medal Standings" .  Pyeongchang 2018 Paralympics . Retrieved  9 March  2018 .   ^   "International Paralympic Committee and Olympic Channel join forces to cover Pyeongchang 2018" .  Inside the Games . Retrieved  2018-03-10 .   ^   "EBU - EBU brings the best of Paralympic sport to over 25 countries across Europe" .  European Broadcasting Union . Retrieved  9 March  2018 .   ^   "How to Watch—And What to Expect From—the Winter Paralympics 2018 on NBC" .  Sports Illustrated . Retrieved  8 March  2018 .   ^   "Why do Americans ignore the Paralympics?" .  The Boston Globe . Retrieved  9 March  2018 .   ^   "CBC/Radio-Canada offer 600-plus hours of Paralympic coverage" .  CBC Sports . Retrieved  9 March  2018 .   ^   "Channel 4 to broadcast 100 hours of Paralympics from Pyeongchang 2018" .  Inside the Games . Retrieved  2018-03-10 .   ^  a   b   "PyeongChang 2018 ticket prices announced" .  www.paralympic.org . Retrieved  8 June  2017 .   ^   "PyeongChang 2018 Paralympic Winter Games Ticket Prices Announced" . Retrieved  9 March  2018 .   ^   "평창패럴림픽 티켓 판매율 0.2%…관심 절실"  [PyeongChang Paralympics Ticket Selling Rate 0.2% ... An urgent concern]. Yonhapnews. 27 October 2017.   ^   "Pyeongchang 2018 unveil ticket design and open offline sales in bid to encourage buyers" . 9 February 2018 . Retrieved  9 February  2018 .   ^   "Pyeongchang 2018 Olympic ticket sales rise to 61 per cent" . Retrieved  9 February  2018 .   ^   "Pyeonchang 2018 unveils official Paralympic emblem" . 29 October 2013 . Retrieved  9 February  2018 .   ^   "PyeongChang 2018 Launches Official Emblem" .  olympic.org .  International Olympic Committee . Retrieved  12 August  2016 .   ^   "Tiger and bear mascots unveiled for Pyeongchang 2018 Winter Olympics and Paralympics" . 2 June 2016 . Retrieved  9 March  2018 .   ^   "Stars invite fans to join them at Paralympics" .  Korea JoongAng Daily . Retrieved  7 March  2018 .   ^   "Jang Keun-suk and Lee Dong-wook in 'Christmas in March' for Paralympics" .  Korea Herald . Retrieved  7 March  2018 .   ^   "Actor Lee Dong-wook named honorary ambassador for PyeongChang Olympics" .  Yonhap News Agency . Retrieved  7 March  2018 .   ^   Herald, The Korea (4 March 2018).  "[PyeongChang 2018] S. Korea to hold hallyu festival to boost Paralympics" . Retrieved  7 March  2018 .   ^   "Festival celebrating Korean culture launched to help promote Winter Paralympic Games" .  Inside the Games . Retrieved  7 March  2018 .   ^   " ' Flag dispute' halts joint Korean march" .  BBC News . 2018-03-08 . Retrieved  2018-03-10 .   ^   "North, South Korea to march separately in Paralympics opening ceremony" .  CBC Sports . Retrieved  8 March  2018 .   
 
 
 External links [ edit ] 
 Pyeongchang 2018 web site 
  Media related to  2018 Winter Paralympics  at Wikimedia Commons 
 Preceded  by 
 Sochi Winter Paralympics 
 PyeongChang 
 XII Paralympic Winter Games  (2018) Succeeded  by 
 Beijing 
 
 
 
 v t e 
 
 Paralympic Games 
 
 
 Charter Host cities IPC 
 NPCs 
 Medal tables Medalists Participating nations
 Summer Winter 
 Sports Symbols 
 
 Summer Games 
 
 
 Rome 1960 Tokyo 1964 Tel Aviv 1968 Heidelberg 1972 Toronto 1976 Arnhem 1980 New York/Stoke Mandeville 1984 Seoul 1988 Barcelona/Madrid 1992 Atlanta 1996 Sydney 2000 Athens 2004 Beijing 2008 London 2012 Rio de Janeiro 2016 Tokyo 2020 Paris 2024 Los Angeles 2028 
 
 
 
 
 Winter Games 
 
 
 Örnsköldsvik 1976 Geilo 1980 Innsbruck 1984 Innsbruck 1988 Tignes/Albertville 1992 Lillehammer 1994 Nagano 1998 Salt Lake City 2002 Turin 2006 Vancouver 2010 Sochi 2014 Pyeongchang 2018 Beijing 2022 TBD 2026 
 
 
 
 
 Olympic Games Youth Olympic Games Ancient Olympic Games 
 
 
 
 
 
 
 v t e 
 
 Nations at the  2018 Winter Paralympics  in  Pyeongchang ,  South Korea 
 America 
 
 Argentina Brazil Canada Chile Mexico United States 
 
 Asia 
 
 China Iran Japan Kazakhstan Mongolia North Korea South Korea Tajikistan Uzbekistan 
 
 Europe 
 
 Andorra Armenia Austria Belarus Belgium Bosnia and Herzegovina Bulgaria Croatia Czech Republic Denmark Finland France Georgia Germany Great Britain Greece Hungary Iceland Italy Netherlands Norway Poland Romania Serbia Slovakia Slovenia Spain Sweden Switzerland Turkey Ukraine 
 
 Oceania 
 
 Australia New Zealand 
 
 Others 
 
 Neutral Paralympic Athletes 
 
 
 
 
 
 
 v t e 
 
 Events  at the  2018 Winter Paralympics  ( Pyeongchang ) 
 
 
 Alpine skiing Biathlon Cross-country skiing Para ice hockey Snowboarding Wheelchair curling 
 
 
 
 Chronological summary Medal table List of medalists 
 
 
 
 
 
 
 v t e 
 
 Venues of the 2018 Winter Paralympics 
 Pyeongchang 
(mountain cluster) 
 
 Alpensia Resort 
 
 Alpensia 
 Biathlon Centre Cross-Country Centre Convention Centre International Broadcast Centre 
 Pyeongchang Olympic Stadium Pyeongchang Olympic Village 
 
 Stand-alone venues 
 
 Jeongseon Alpine Centre 
 
 
 Gangneung 
(coastal cluster) 
 
 Gangneung Olympic Park 
 Gangneung Curling Centre Gangneung Hockey Centre 
 Gangneung Olympic Village 
 
 
 
 Italics  indicate non-competition venues. 
 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=2018_Winter_Paralympics &oldid=830353067 "					 
				 Categories :  2018 in multi-sport events 2018 Winter Paralympics Winter Paralympic Games Sport in Pyeongchang County Current sports events 2018 in winter sports 2018 in South Korean sport International sports competitions hosted by South Korea Sport in Gangneung Multi-sport events in South Korea March 2018 sports events in Asia Winter sports competitions in South Korea Hidden categories:  Webarchive template wayback links Use dmy dates from March 2018 Articles containing Korean-language text Pages using div col with deprecated parameters All articles with unsourced statements Articles with unsourced statements from March 2018 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Article Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read Edit View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 In other projects 
			 
								 Wikimedia Commons 
							 
		 
			 
			 Languages 
			 
								 Afrikaans العربية বাংলা Čeština Dansk Deutsch Ελληνικά Español فارسی Français Frysk 한국어 हिन्दी Hrvatski Bahasa Indonesia Italiano עברית Latviešu Bahasa Melayu Nederlands 日本語 Norsk ଓଡ଼ିଆ پښتو Polski Português Русский Simple English Slovenčina Suomi Svenska తెలుగు ไทย Українська Tiếng Việt 中文 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 14 March 2018, at 09:08. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"1.220","walltime":"1.411","ppvisitednodes":{"value":12446,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":181552,"limit":2097152},"templateargumentsize":{"value":15285,"limit":2097152},"expansiondepth":{"value":12,"limit":40},"expensivefunctioncount":{"value":4,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":47254,"limit":5000000},"entityaccesscount":{"value":1,"limit":400},"timingprofile":["100.00% 1221.987      1 -total"," 31.64%  386.693    187 Template:Country_alias"," 26.07%  318.612     49 Template:FlagIPC"," 25.70%  314.059     49 Template:FlagIOC2"," 21.21%  259.186      1 Template: Reflist"," 16.40%  200.369      1 Template:Korean"," 14.28%  174.507      3 Template:Lang","  9.18%  112.150      1 2018_Winter_Paralympics_medal_table","  9.10%  111.172     19 Template:Cite_web","  8.09%   98.906     24 Template:Cite_news"]},"scribunto":{"limitreport-timeusage":{"value":"0.720","limit":"10.000"},"limitreport-memusage":{"value":20782171,"limit":52428800}},"cachereport":{"origin":"mw1321","timestamp":"20180314125943","ttl":1900800,"transientcontent":false}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":111,"wgHostname":"mw1323"});});
Wikipedia:Your first article 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 
 
 Shortcut 
 WP:YFA 
 
 
 
 Writing an article 
 Learn how you can create an article. 
 
 
 This page is not a sandbox . It should not be used for test editing. 
To experiment, you can use the shared  sandbox  – or if you're logged in,  your personal sandbox . 
 
 This page in a nutshell:  Wikipedia articles follow certain guidelines: the topic should be  notable  and be covered in detail in  good references  from  independent sources . Wikipedia is an  encyclopedia  –  it is not a personal home page or a business list . Do not use content from other websites even if you, your school, or your boss owns them. If you choose to create the article with only a limited knowledge of the standards here, you should be aware that other editors may delete it if it's not considered appropriate. To create full articles (as opposed to draft pages), your account must be at least 4 days (96 hours) old, and y ou must have made more than ten edits. For information on how to request a new article that can be created by someone else, see  Wikipedia:Requested articles . To create an article, you can try the  Article Wizard . 
 Welcome to Wikipedia!  You have probably already edited blogs or social media sites. Maybe you have already made minor edits to our articles – but now you want to start a new article from scratch. 
 Wikipedia:Processes Article creation Introductory 
 Getting started with Wikipedia Article wizard Your first article 
 Suggested articles 
 Most-wanted articles Requested articles Images needing articles 
 Concepts and guidelines 
 Standard layout Lead section Sections Stub articles Categorization 
 Development processes 
 Article development Moving a page Merging articles Featured article criteria The perfect article 
 Meta tools and groups 
 WikiProject: Articles for creation Special:NewPages New pages patrol New articles by topic Recent additions  ( DYK ) 
 
 
 v t e 
 
 
 
 
 
 Contents 
 
 1   Introduction 2   Search for an existing article 3   Gathering references 4   Things to avoid 5   And be careful about... 6   Are you closely connected to the article topic? 7   Create your draft 8   And then what? 
 8.1   Keep making improvements 8.2   Improve formatting 8.3   Avoid orphans 8.4   Add to a disambiguation page 
 9   Still need help? 
 9.1   Read a traditional encyclopedia 
 
 
 
 Introduction 
 First, please be aware that Wikipedia is an  encyclopedia , and our mission is to share accepted knowledge to benefit people who want to learn. We are not social media or a place to promote a company or product or person, or a place to advocate for or against anyone or anything. Please keep this in mind, always. (This is described in our "mission statement",  "What Wikipedia is not" .) 
 We find "accepted knowledge" in high quality, published sources. By "high quality" we mean books by reputable publishers, high-quality newspapers like  The New York Times , or  literature reviews  in the scientific literature. We  summarize  such sources here. That is all we do! Please make sure that anything you write in Wikipedia is based on such sources - not what is in your head. 
 Here are some tips that can help you with your first article: 
 Consider  registering an account  first—you only need to choose a username and password. (This will help you find things you create, and help other people find you) Search Wikipedia first in case an article  already exists  on the subject, perhaps under a different title. If the article already exists, feel free to make any constructive edits to improve it. 
 
 
 Is it new? 
 Type, then click "Go (try title)" 
 
 
 
   
 
 
 
 
 
 Nothing? OK, now you need to try to determine if the subject you want to write about is what we call  "notable"  in Wikipedia. The question we ask is -  does this topic belong in an  encyclopedia ? 
 
 We generally judge this by asking if there are at least three high-quality sources that a) have substantial discussion of the subject (not just a mention)  and  b) are written and published independently of the subject (so, a company's website or press releases are not OK). Everything here is based on high-quality independent sources, and without them, we generally just cannot write an article. If you are not sure if the subject you want to write about is "notable", you can ask questions at the Wikipedia  Teahouse . Please be mindful of  conflict of interest . If you have one, you will probably have a hard time writing a good enough Wikipedia article (this is not about you, it is just human nature). However, if you insist on trying, you need to disclose your conflict of interest, and you need to try very hard not to allow your "external interest" to drive you to abuse Wikipedia. And you need to try hard to  hear  the feedback from independent people who review the draft before it is published and made available in the main encyclopedia. Your conflict of interest might lead you to believe something is "notable" when it isn't and to argue too hard for it to be published there. 
 
 Practice first.  Before starting, try editing existing articles to get a feel for writing and for using Wikipedia's mark-up language—we recommend that you first take a tour through the  Wikipedia tutorial  or review  contributing to Wikipedia  to learn editing basics. 
 The  Article Wizard  will help you create your article in Draft space, and will put some useful templates into your draft, including the button to click when you are ready to submit the draft for review. 
 Article Wizard 
 An easy way to create articles. 
 These points are explained in further detail below. 
 If you are logged in, and your account is  autoconfirmed , you can also use this box below to create an article, by entering the article name in the box below and then clicking "Create page". 
 
 
 
 
 
 Search for an existing article 
 Wikipedia already has 5,587,614 articles. Before creating an article, try to make sure there is not already an article on the same topic, perhaps under a slightly different name.  Search  for the article, and review Wikipedia's  article titling policy  before creating your first article. If an article on your topic already exists, but you think people might look for it under some different name or spelling, learn how to  create  redirects  to alternative titles ; adding needed redirects is a good way to help Wikipedia. Also, remember to check the article's  deletion log  in order to avoid creating an article that has already been deleted. (In some cases, the topic may be suitable even if deleted in the past; the past deletion may have been because it was a copyright violation, did not explain the importance of the topic, or on other grounds addressed to the writing rather that the topic's suitability.) 
 If a search does not find the topic, consider broadening your search to find existing articles that might include the subject of your article. For example, if you want to write an article about a band member, you might search for the band and then add information about your subject as a  section  within that broader article. 
 
 Gathering references 
 Notability Subject-specific guidelines 
 
 Academics Astronomical objects 
 Books Events 
 Films Geographic features 
 Military Music Numbers 
 Organizations and companies 
 People Sports and athletes 
 Web content 
 
 See also 
 Wikipedia essays Guide to deletion Common deletion outcomes Why was my article deleted? 
 
 
 v t e 
 
 
 Gather sources for the information you will be writing about. To be worthy of inclusion in an encyclopedia, a subject must be sufficiently  notable , and that notability must be  verifiable  through  citations  to  reliable sources . 
 As noted, the sources you use must be  reliable ; that is, they must be sources that exercise some form of editorial control and have some reputation for fact-checking and accuracy. Print sources (and web-based versions of those sources) tend to be the most reliable, though some web-only sources may also be reliable. Examples might include (but are not limited to) books published by major publishing houses, newspapers, magaz ines, peer-reviewed scholarly journals, websites of any of the above, and other websites that meet the same requirements as a reputable print-based source. 
 In general, sources with  no  editorial control are not reliable. These include (but are not limited to) books published by vanity presses, self-published 'zines', blogs, web forums, usenet discussions, personal social media, fan sites, vanity websites that permit the creation of self-promotional articles, and other similar venues. If anyone at all can post information without anyone else checking that inf ormation, it is probably not reliable. 
 To put it simply, if there are reliable sources (such as newspapers, journals, or books) with extensive information published over an extended period about a subject, then that subject is notable and you must cite such sources as part of the process of creating (or expanding) the Wikipedia article. If you cannot find such  reliable sources  that provide extensive and comprehensive information about your proposed subject, then the subject is not notable or verifiable and almost certainly will be deleted. So your first job is to  go find references  to cite. 
 There are many places to find reliable sources, including your local library, but if internet-based sources are to be used, start with  books  and  news archive  searches rather than a web search. 
 Once you have references for your article, you can learn to place the references into the article by reading  Help:Referencing for beginners  and  Wikipedia:Citing sources . Do not worry too much about formatting citations properly. It would be great if you did that, but the main thing is to  get references into the article , even if they are not perfectly formatted. 
 Things to avoid 
 Main pages:  Wikipedia:What Wikipedia is not  and  Wikipedia:Avoiding common mistakes 
 Articles about yourself, your family or friends, your website, a band you're in, your teacher, a word you made up, or a story you wrote   If you  are  worthy of inclusion in the encyclopedia, let someone else add an article for you. Putting your friends in an encyclopedia may seem like a nice surprise or an amusing joke, but articles like this are likely to be  removed . In this process, feelings may be hurt and you may be  blocked  from editing if you repeatedly make attempts to re-create the article. These things can be avoided by a little forethought on your part. The article may remain if you have enough humility to make it neutral and you really are notable, but even then it's best to submit a draft for approval and  consensus  of the community instead of just posting it up, since unconscious biases may still exist of which you may not be aware. Advertising   Please do not  try to promote your product or business . Please do not insert external links to your commercial website unless a neutral party would judge that the link truly belongs in the article; we do have articles about products like  Kleenex  or  Sharpies , or notable businesses such as  McDonald's , but if you are writing about a product or business be sure you write from a  neutral point of view , that you have no  conflict of interest , and that you are able to find references in  reliable sources  that are independent from the subject you are writing about. Attacks on a person or organization   Material that violates our  biographies of living persons  policy or is intended to threaten, defame, or harass its subject or another entity is not permitted. Unsourced negative information, especially in articles about living people, is quickly removed, and  attack pages  may be deleted immediately. Personal essays or original research   Wikipedia surveys  existing  human knowledge; it is not a place to publish new work. Do not write articles that present your own  original theories, opinions, or insights,   even if  you can support them by reference to accepted work. A common mistake is to present a novel synthesis of ideas in an article. Remember, just because both Fact A and Fact B are true does not mean that A caused B, or vice versa ( fallacies ) or ( Post hoc ergo propter hoc ). If the synthesis or causation is true, locate and cite  reliable sources  that report the connection. Non-notable topics   People frequently add pages to Wikipedia without considering  whether the topic is really notable enough  to go into an encyclopedia. Because Wikipedia does not have the space limitations of paper-based encyclopedias, our  notability  policies and guidelines allow a wide range of articles – however, they do not allow  every  topic to be included. A particularly common special case of this is pages about people, companies, or groups of people, that do not substantiate the notability or importance of their subject with reliable sources, so we have decided that such pages may be speedily deleted under our  WP:SPEEDY  policy. This can offend – so  please  consider whether your chosen topic is notable enough for Wikipedia, and then substantiate the notability or importance of your subject by citing those reliable sources in the process of creating your article.  Wikipedia is not  a directory of everything in existence. A single sentence or only a website link Articles need to have real content of their own. See also: 
 List of bad article ideas 
 And be careful about... 
 
 Copyright 
 
 As a general rule,  do not copy-paste text from other websites .  (There are a few limited exceptions, and a few words as part of a properly  cited  and clearly attributed quotation is OK.) 
 
 –  Wikipedia:Copy-paste 
 Copying things. Do not violate copyrights Never copy and paste text into a Wikipedia article unless it is a relatively short quotation, placed in quotation marks, and cited using an  inline citation . Even material that you are  sure  is in the  public domain  must be attributed to the source, or the result, while not a copyright violation, is  plagiarism . Also, note that most web pages  are not  in the public domain and most song  lyrics   are not  either. In fact, most things published after 1923, and almost all works written since  January 1 ,  1978 , are automatically protected by  copyright  under the  Copyright Act of 1976   even if they have no copyright notice or © symbol.  If you think what you are contributing is in the public domain,  say where you got it,  either in the article or on the discussion page, and on the discussion page give the reason why you think it is in the public domain (e.g. "It was published in 1895..."). For more information, see  Wikipedia:Copyrights  (which includes instructions for verifying permission to copy previously published text) and  our non-free content guidelines for text . Finally, please note that superficial modification of material, such as minor rewording, is insufficient to avoid plagiarism and copyright violations. See  Wikipedia:Close paraphrasing . 
 
 Good sources 
 
 1. have a reputation for reliability: they are  reliable sources 
 2. are independent of the subject 
3. are  verifiable  by other editors 
 
 
 Good research and citing your sources Articles written out of thin air may be better than nothing, but they are hard to  verify , which is an important part of building a trusted reference work. Please research with the  best sources available  and  cite them  properly. Doing this, along with not copying text, will help avoid any possibility of  plagiarism . We welcome  good  short articles, called " stubs ", that can serve as launching pads from which others can take off – stubs can be relatively short, a few sentences, but should provide some useful information. If you do not have enough material to write a good stub, you probably should not create an article. At the end of a stub, you should include a "stub template" like this: {{stub}}. (Other Wikipedians will appreciate it i f you use a more specific stub template, like {{art-stub}}. See the  list of stub types  for a list of all specific stub templates.) Stubs help track articles that need expansion. 
 Articles about  living persons Articles written about living persons must be referenced so that they can be  verified . Biographies about living subjects that lack sources may be deleted. 
 Advocacy and controversial material Please do not write articles that advocate one particular viewpoint on politics, religion, or anything else. Understand what we mean by a  neutral point of view  before tackling this sort of topic. 
 Articles that contain different definitions of the topic Articles are primarily about what something  is ,  not  any term(s). If the article is  just about a word or phrase  and especially if there are very different ways that a term is used, it usually belongs in  Wiktionary . Instead, try to write a good short first paragraph that  defines  one  subject  as well as some more material to go with it. 
 Organization Make sure there are incoming links to the new article from other Wikipedia articles (click "What links here" in the toolbox) and that the new article is included in at least one appropriate category (see  help:category ). Otherwise, it will be difficult for readers to find the article. 
 Local-interest articles These are articles about places like schools, or streets that are of interest to a relatively small number of people such as alumni or people who live nearby.  There is no consensus  about such articles, but some will challenge them if they include nothing that shows how the place is special and different from tens of thousands of similar places. Photographs add interest. Try to give  local-interest articles  local colour.  Third-party sources  are the only way to prove that the subject you are writing about is  notable . 
 Breaking news events While Wikipedia accepts articles about notable recent events, articles about breaking news events with no enduring notability are  not appropriate for our project . Consider writing such articles on our sister project  Wikinews . See  Wikipedia:Notability (events)  for further information. 
 Editing on the wrong page If you're trying to create a new page, you'll start with a completely empty edit box. If you see text in the editing box that is filled with words you didn't write (for example, the contents of this page), you're accidentally editing a pre-existing page. Don't "Publish changes" your additions. See  Wikipedia:How to create a page , and start over. 
 Are you closely connected to the article topic? 
 Wikipedia is the encyclopedia that anyone can edit, but there are special guidelines for editors who are paid or sponsored. These guidelines are intended to prevent biased articles and maintain the public's trust that content in Wikipedia is impartial and has been added in good faith. (See Wikipedia's  conflict of interest (COI)  guideline.) 
 The official guidelines are that editors  must be volunteers.  That means Wikipedia discourages editing articles about individuals, companies, organizations, products/services, or political causes that pay you directly or indirectly. This includes in-house PR departments and marketing departments, other company employees,  public relations  firms and publicists, social media consultants, and  online reputation management  consultants. However, Wikipedia recognizes the large volume of good faith contributions by people who have some affiliation to the articles they work on. 
 Here are some ground rules. If you break these rules, your edits are likely to be reverted, and the article(s) and your other edits may get extra scrutiny from other Wikipedia editors. Your account may also be blocked. 
 Things to avoid Things to be careful about Great ways to contribute 
 Don't add promotional language Don't remove negative/critical text from an article Don't make a "group" account for multiple people to share Don't neglect to disclose your affiliation on the article's talk page 
 
 Maintain a neutral, objective tone in any content you add or edit Cite  independent, reliable sources  (e.g., a major media article) for any new statements you add – even if you are confident a statement is true (e.g., it is about your work), only say it if it has been restated already in a  reliable source . 
 
 Make minor edits/corrections to articles (e.g., typos, fixing links, adding references to reliable sources) If you are biased, suggest new article text or edits on the  article talk page  (not on the main article page). Disclose your relationship to the client/topic. Edit using personal accounts. Recruit help: Seek out a sponsor (volunteer editor) who has worked on similar articles, or submit ideas for article topics via  Requested articles . 
 
 Note that this has to do only with conflict of interest. Editors are encouraged to write on topics related to their expertise: e.g., a NASA staffperson might write about planets, or an academic researcher might write about their field. Also,  Wikipedians-in-residence  or other interns who are paid, hosted or otherwise sponsored by a scientific or cultural institution can upload content and write articles in partnership with curators, indirectly providing positive branding for their hosts. 
 Create your draft 
 Click here:  Article wizard , read the brief introduction, and then click the big blue button to get started creating your draft. 
 And then what? 
 Now that you have created the page, there are still several things you can do. 
 Keep making improvements 
 Wikipedia is not finished . Generally, an article is nowhere near being completed the moment it is created. There is a long way to go. In fact, it may take you several edits just to get it started. 
 If you have so much interest in the article you just created, you may learn more about it in the future, and accordingly, have more to add. This may be later today, tomorrow, or several months from now. Any time – go ahead. 
 Improve formatting 
 To format your article correctly (and expand it, and possibly even make it  featured !), see 
 Wikipedia:Tutorial  to learn how to format your article Wikipedia:Writing better articles Wikipedia:The perfect article Wikipedia:Lead section 
 Others can freely contribute to the article when it has been saved. The creator does not have special rights to control the later content. See  Wikipedia:Ownership of articles . 
 Also, before you get frustrated or offended about the way others modify or remove your contributions, see  Wikipedia:Don't be ashamed . 
 Avoid orphans 
 An  orphaned article  is an article that has few or no other articles linking to it. The main problem with an orphan is that it'll be unknown to others, and may get fewer readers if it is not de-orphaned. 
 Most new articles are orphans from the moment they are created, but you can work to change that. This will involve editing one or more  other  articles. Try searching Wikipedia for other pages referring to the subject of your article, then turn those references into links by adding double brackets to either side: "[[" and "]]". If another article has a word or phrase that has the same meaning as your new article, but not expressed in the same words as the title, you can link that word or phrase as follows: "[[title o f your new article|word or phrase found in other article]]." Or in certain cases, you could create that word or phrase as a redirect to your new article. 
 One of the first things you want to do after creating a new article is to provide links to it so it will not be an orphan. You can do that right away, or if you find that exhausting, you can wait a while, provided that you keep the task in mind. 
 See  Wikipedia:Drawing attention to new pages  to learn how to get others to see your new articles. 
 Add to a disambiguation page 
 If the term is ambiguous (meaning there are multiple pages using that or a similar title), see if there is a  disambiguation page  for articles bearing that title. If so, add it to that page. 
 Still need help? 
   Help desk 
 For a list of informative, instructional and supportive pages, see  Help directory . The best places to ask for assistance is at the  Teahouse  and at the main  Help desk . Click here to ask for help on your talk page , a volunteer will visit you there shortly! For a list of the services and assistance that can be requested on Wikipedia, see  Request departments . Alternately you can ask a question through the Wikipedia  #wikipedia-en-help   connect  on IRC chat. 
 Read a traditional encyclopedia 
 Try to read traditional paper encyclopedia articles to get the layout, style, tone, and other elements of encyclopedic content. It is suggested that if you plan to write articles for an encyclopedia, you have some background knowledge in formal writing as well as about the topic at hand. A composition class in your high school or college is recommended before you start writing  encyclopedia articles. 
 The  World Book  is a good place to start. The goal of Wikipedia is to create an up-to-the-moment encyclopedia on every notable subject imaginable. Pretend that your article will be published in a paper encyclopedia. 
 
 
 
 v t e 
 
   Basic information  on  Wikipedia 
 
 
 Main Help 
 directory menu 
 FAQs Reference  desk Help desk 
 
 About Wikipedia 
 
 Administration 
 Purpose Who writes Wikipedia? Organization 
 Censorship Introduction Why create an account? In brief General disclaimer What Wikipedia is not 
 
 Readers' FAQ 
 
 Parental advice Navigation 
 Introduction 
 Searching Viewing media 
 Help 
 Mobile access Other languages Researching with Wikipedia 
 Citing Wikipedia 
 Students help Readers' index Copyright Book creation 
 
 Contributing 
to Wikipedia 
 
 Main tutorial 
 Tutorials and introductions 
 The answer Dos and don'ts Learning the ropes Common mistakes Newcomer primer Plain and simple Your first article 
 Wizard 
 Young Wikipedians 
 The Adventure 
 
 
 Protocols 
and conventions 
 
 Five pillars 
 Introduction Simplified ruleset Simplified MoS 
 Etiquette 
 Expectations Oversight 
 Principles Ignore all rules 
 The rules are principles 
 Core content policies Policies and guidelines 
 Policies Guidelines 
 Vandalism Appealing blocks 
 
 Getting assistance 
 
 Requests for help 
 Request editor assistance Disputes resolution requests 
 IRC live chat 
 Tutorial 
 Village pump Contact us 
 
 Wikipedia community 
 
 Community portal Dashboard 
 Noticeboards 
 Departments Maintenance 
 Task Center 
 Essays Meetups WikiProjects 
 
 Sourcing 
and  referencing 
 
 Finding sources Combining sources Referencing 
 Introduction 
 Citations 
 Citation Style 1 Citation templates Footnotes Page numbers Cite errors 
 
 
 Information 
 
 Editing 
 Toolbar Conflict 
 VisualEditor 
 User guide 
 Category Diffs Email confirmation Infoboxes Linking 
 Link color 
 Manual of Style 
 Introduction Simplified 
 Namespaces Page name URLs User contribution pages Using talk pages 
 Introduction Archiving 
 Image and media files 
 Images Media files 
 
 
 How-to 
 
 Guide to page deletion 
 Image deletion 
 Logging in Merging pages Page renaming 
 Requests 
 Redirecting Reset passwords Reverting Uploading images 
 Introduction 
 
 
 Wiki markup 
 
 Wiki markup 
 Cheatsheet 
 Barcharts Calculations Columns HTML Lists Magic words 
 For beginners 
 Music symbols Sections Sounds Special Characters Tables 
 Introduction 
 Templates 
 Documentation Messages 
 Tools Transclusion Visual file markup 
 Tutorial 
 
 
 Directories 
 
 Abbreviations Contents Edit summaries Essays Glossary Index The Missing Manual Shortcuts Tips 
 Tip of the day 
 Wikis 
 
 
 
 Teahouse  (interactive help for new editors) Ask for help on your talk page  (a volunteer will visit you there) 
 
 
 
 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=Wikipedia:Your_first_article &oldid=829962911 "					 
				 Categories :  Wikipedia basic information Wikipedia how-to Wikipedia page help Wikipedia tips Wikipedia new articles Hidden categories:  Wikipedia semi-protected project pages Wikipedia move-protected project pages 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Project page Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read View source View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 Languages 
			 
								 Afrikaans العربية বাংলা Bân-lâm-gú भोजपुरी Български Català Čeština Chavacano de Zamboanga Cymraeg Dansk Ελληνικά Español Esperanto فارسی Հայերեն हिन्दी Hrvatski Bahasa Indonesia עברית Basa Jawa ქართული Ladino Magyar मैथिली Bahasa Melayu မြန်မာဘာသာ 日本語 Norsk ଓଡ଼ିଆ ਪੰਜਾਬੀ پښتو Ripoarisch Română Русиньскый Sicilianu සිංහල سنڌي Slovenčina Soomaaliga کوردی Српски / srpski Basa Sunda Suomi Svenska தமிழ் ไทย Türkçe اردو Tiếng Việt 中文 Беларуская (тарашкевіца)‎ 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 11 March 2018, at 21:58. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.212","walltime":"0.315","ppvisitednodes":{"value":1289,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":75903,"limit":2097152},"templateargumentsize":{"value":5234,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":5,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":964,"limit":5000000},"entityaccesscount":{"value":0,"limit":400},"timingprofile":["100.00%  241.411      1 -total"," 28.06%   67.751      1 Template:Active_editnotice"," 23.12%   55.817      1 Template:Editnotices/Page/Wikipedia:Your_first_article"," 22.70%   54.801      1 Template:Article _creation_editnotice"," 21.75%   52.508      1 Template:Editnotice"," 18.05%   43.564      1 Template:Pp-semi-indef"," 17.79%   42.957      1 Template:Fmbox","  9.93%   23.974      1 Template:Shortcut","  8.26%   19.949      2 Template:Sidebar","  6.50%   15.696      1 Template:IncGuide"]},"scribunto":{"limitreport-timeusage":{"value":"0.072","limit":"10.000"},"limitreport-memusage":{"value":2753789,"limit":52428800}},"cachereport":{"origin":"mw1343","timestamp":"20180311215804","ttl":3600,"transientcontent":true}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":93,"wgHostname":"mw1264"});});
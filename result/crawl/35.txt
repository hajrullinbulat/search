Nanakshahi calendar 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 
 
 
 This article  needs additional citations for  verification .  Please help  improve this article  by  adding citations to reliable sources . Unsourced material may be challenged and removed.   (March 2018)   ( Learn how and when to remove this template message ) 
 
 The  Nanakshahi  ( Punjabi :  ਨਾਨਕਸ਼ਾਹੀ ,  nānakashāhī ) Calendar is a tropical  solar calendar  and the year is based on the 'Barah Mah' which were composed by the Sikh Gurus. Barah Maha translates as the "Twelve months” which is a poem reflecting the changes in nature which are conveyed in the twelve-month cycle. [1]  The years begins with the month of 'Chet' with 01 Chet corresponding with 14 March CE. The year one of Nanakshahi calendar starts in 1469 CE-the year of the birth of Guru Nanak. [2] 
 
 
 
 Contents 
 
 1   Etymology 2   History 3   Features 4   Months 5   Festivals and events 6   See also 7   References 8   External links 
 
 
 Etymology [ edit ] 
 The term Nanakshahi is rooted in the name of the first Guru Nanak Dev. 
 History [ edit ] 
 Sikhs have traditionally recognised two eras and calendars: the Nanakshahi and Khalsa. Traditionally, both these calendars closely followed the  Bikrami  calendar but the Nanakshahi Sammat began on Kattak puranmashi (full moon). [3]  The Nanakshahi calendar was adopted by the  Shiromani Gurdwara Prabhandak Committee  to determine the dates for important  Sikh  events. The calendar was implemented during the SGPC presidency of Sikh scholar Prof. Kirpal Singh Badungar at Takhat Sri Damdama Sahib in the presence of Sikh leadership. [4]  It was designed by Pal Singh Purewal to replace the  Bikrami calendar  and has been in use since 1998. The  epoch  of this calendar is the birth of the first  Sikh Guru ,  Nanak Dev  in 1469.  New Year's Day  falls annually on what is March 14 in the  Gregorian  Western calendar. [4] The calendar is accepted in about 90% of the  gurdwaras  throughout the world. There is some controversy about the acceptance of the calendar among certain sectors of the Sikh world. [5]  Some organizations and factions that have not accepted include the Sant Samaj,  Damdami Taksal , and Buddha Dal  Nihangs . 
 Features [ edit ] 
 Features of the New Calendar: [5] 
 Uses the accurate  Tropical year  (365 Days, 5 Hours, 48 Minutes, 45 Seconds) rather than the  Sidereal year Called Nanakshahi after  Guru Nanak  (Founder of  Sikhism ) Year 1 is the Year of Guru Nanak's Birth (1469 C.E.). As an example, March 14, 2018 CE is Nanakshahi 550. Is Based on  Gurbani [6]  - Month Names are taken from  Guru Granth Sahib [7] Contains 5 Months of 31 days followed by 7 Months of 30 days Leap year every 4 Years in which the last month (Phagun) has an extra day Approved by  Akal Takht  in 2003 [8] 
 Months [ edit ] 
 The months in the Nanakshahi calendar are: [4] [6] 
 No. Name Punjabi Days Gregorian Months 1 Chet ਚੇਤ 31 14 March – 13 April 2 Vaisakh ਵੈਸਾਖ 31 14 April – 14 May 3 Jeth ਜੇਠ 31 15 May – 14 June 4 Harh ਹਾੜ 31 15 June – 15 July 5 Sawan ਸਾਵਣ 31 16 July – 15 August 6 Bhadon ਭਾਦੋਂ 30 16 August – 14 September 7 Assu ਅੱਸੂ 30 15 September – 14 October 8 Katak ਕੱਤਕ 30 15 October – 13 November 9 Maghar ਮੱਘਰ 30 14 November – 13 December 10 Poh ਪੋਹ 30 14 December – 12 January 11 Magh ਮਾਘ 30 13 January – 11 February 12 Phagun ਫੱਗਣ 30/31 12 February – 13 March 
 Festivals and events [ edit ] 
 Festivals and events [9] Nanakshahi date Gregorian date Guru Har Rai  becomes the seventh Guru of the Sikhs 
 Nanakshahi  New Year Commences 1  Chet 14 Mar Guru Hargobind , the sixth Sikh Guru, dies 6  Chet 19 Mar The ordination of the  Khalsa 
Birth of  Guru Nanak  (Vaisakhi Date) [10] 1  Vaisakh 14 Apr Guru Angad , the second Sikh Guru, merges back to the creator 3  Vaisakh 16 Apr Guru Amar Das  becomes the third Guru of the Sikhs 3  Vaisakh 16 Apr Guru Harkrishan , the eighth Guru of the Sikhs, dies 3  Vaisakh 16 Apr Guru Tegh Bahadur  becomes the Ninth Guru of the Sikhs 3  Vaisakh 16 Apr Birth of  Guru Angad , the second Sikh Guru 5  Vaisakh 18 Apr Birth of  Guru Tegh Bahadur , the ninth Sikh Guru of the Sikhs 5  Vaisakh 18 Apr Birth of  Guru Arjan , the fifth Sikh Guru 19  Vaisakh 2 May Birth of  Guru Amar Das , the third Sikh Guru 9  Jeth 23 May Guru Hargobind  becomes the sixth Guru of the Sikhs 28  Jeth 11 Jun Guru Arjan , the fifth Guru of the Sikhs, in martyred by  Chandu Shah  in Lahore 2  Harh 16 Jun Birth of  Guru Hargobind , the sixth Sikh Guru 21  Harh 5 Jul Guru Hargobind  Sahib 6  Sawan 21 Jul Guru Harkrishan  Sahib 8  Sawan 23 Jul Guru Granth Sahib , the  Sikh Scripture , is installed at the  Golden Temple  for the first time 17  Bhadon 1 Sep Guru Amar Das , the third Guru of the Sikhs, dies 2  Assu 16 Sep Guru Ram Das  becomes the fourth Guru of the Sikhs 2  Assu 16 Sep Guru Ram Das , the fourth Guru of the Sikhs, dies 2  Assu 16 Sep Guru Arjan  becomes the fifth Guru of the Sikhs 2  Assu 16 Sep Guru Angad  becomes the second Guru of the Sikhs 4  Assu 18 Sep Guru Nanak , the first Guru of the Sikhs, dies 8  Assu 22 Sep Birth of  Guru Ram Das , the fourth Guru of the Sikhs 25  Assu 9 Oct Guru Har Rai , the seventh Guru of the Sikhs, dies 6  Katak 20 Oct Guru Harkrishan  becomes the eighth Guru of the Sikhs 6  Katak 20 Oct Sovereignty of the  Sikh Scripture  ( Guru Granth Sahib ) declared as the Guru for all times to come by  Guru Gobind Singh , the tenth and the last Sikh Guru 6  Katak 20 Oct Guru Gobind Singh , the tenth Sikh Guru, dies 7  Katak 21 Oct Guru Gobind Singh  becomes the tenth Guru of the Sikhs 11  Maghar 24 Nov Guru Tegh Bahadur  martyred in  Delhi  by  Aurangzeb  for defending the oppressed 11  Maghar 24 Nov Ajit Singh , and  Jujhar Singh , the two elder sons of  Guru Gobind Singh , martyred in the  battle of Chamkaur 8  Poh 21 Dec Zorawar Singh , and  Fateh Singh , the two younger sons of  Guru Gobind Singh , executed in  Sirhind 13  Poh 26 Dec Birth of  Guru Gobind Singh , the Tenth  Sikh Guru 23  Poh 5 Jan Birth of  Guru Har Rai , the seventh Sikh Guru 19  Magh 31 Jan 
 Movable dates for Sikh Festivals  (These change every year in line with the Lunar Phase) [11] 
 Year Hola Mohalla Bandi Chhor Divas Guru Nanak Dev Gurpurab 2003 19 Mar 25 Oct 8 Nov 2004 7 Mar 12 Nov 26 Nov 2005 26 Mar 1 Nov 15 Nov 2006 15 Mar 21 Oct 5 Nov 2007 4 Mar 9 Nov 24 Nov 2008 22 Mar 28 Oct 13 Nov 2009 11 Mar 17 Oct 2 Nov 2010 1 Mar 5 Nov 21 Nov 2011 20 Mar 26 Oct 10 Nov 2012 9 Mar 13 Nov 28 Nov 2013 28 Mar 3 Nov 17 Nov 2014 17 Mar 23 Oct 6 Nov 2015 6 Mar 11 Nov 25 Nov 2016 24 Mar 30 Oct 14 Nov 2017 13 Mar 19 Oct 4 Nov 2018 2 Mar 7 Nov 23 Nov 2019 21 Mar 27 Oct 12 Nov 2020 10 Mar 14 Nov 30 Nov 
 See also [ edit ] 
 Bikrami calendar Punjabi calendar Indian national calendar Hindu calendar Bengali calendar 
 References [ edit ] 
 
 
 ^   W. H. McLeod (2009) The A to Z of Sikhism. Scarecrow Press [1] ^   singh, Jagraj (2009) A complete guide to Sikhism. Unistar Books  [2] ^   Singh, Harbans (1998) The Encyclopaedia of Sikhism: S-Z. Publications Bureau  [3] ^  a   b   c   "What is the Sikh Nanakshahi calendar" . allaboutsikhs.com. Archived from  the original  on 2008-05-10 . Retrieved  2008-05-09 .   ^  a   b   "Nanakshahi Calendar at BBC" .  BBC . 2003-07-29 . Retrieved  2008-05-09 .   ^  a   b   Singh Purewal, Pal.  "Gurbani And Nanakshahi Calendar"   (PDF) .  www.purewal.biz . purewal.biz . Retrieved  13 March  2018 .   ^   "Bara Maha - SikhiWiki, free Sikh encyclopedia" .  www.sikhiwiki.org . sikhiwiki.org . Retrieved  13 March  2018 .   ^   Parkash, Chander (14 March 2003).  "Nanakshahi calendar out" .  www.tribuneindia.com . The Tribune . Retrieved  13 March  2018 .   ^   Singh Purewal, Pal.  "Gurpurbs (Fixed Dates)"   (PDF) .  www.purewal.biz . purewal.biz . Retrieved  13 March  2018 .   ^   Singh Purewal, Pal.  "Birth Date of Guru Nanak Sahib"   (PDF) .  www.purewal.biz . purewal.biz . Retrieved  4 February  2018 .   ^   Singh Purewal, Pal.  "Movable Dates of Gurpurbs (Change Every Year)"   (PDF) .  www.purewal.biz . purewal.biz . Retrieved  13 March  2018 .   
 
 
 External links [ edit ] 
 Purewal.biz , the website of Mr. Pal Singh Purewal, the creator of the Nanakshahi Calendar, this site contains detailed articles about this calendar. Nanakshahi Calendar at BBC Gurpurab Nanakshahi Calendar Nanakshahi Day with Holidays and API 
 
 
 
 v t e 
 
   Nanakshahi calendar   
 
 
 Chet Vaisakh Jeth Harh Sawan Bhadon Assu Katak Maghar Poh Magh Phagun 
 
 
 
 
 
 
 v t e 
 
   Sikh   topics   
 Gurus 
 
 Guru Nanak Guru Angad Guru Amar Das Guru Ram Das Guru Arjan Guru Hargobind Guru Har Rai Guru Har Krishan Guru Tegh Bahadur Guru Gobind Singh Guru Granth Sahib  (Sikh holy book) 
 
 Philosophy 
 
 Beliefs and principles Guru Maneyo Granth Sikh Rehat Maryada Prohibitions 
 Cannabis and Sikhism 
 Diet in Sikhism 
 
 Practices 
 
 Khalsa Ardās Kirtan Langar Naam Karan Anand Karaj Amrit Sanchar Amrit Velā Antam Sanskar Three Pillars Kirat Karo Naam Japo Vand Chhako Sikh practices The Five Ks Simran Sewa Charhdi Kala Dasvand Jhatka 
 
 Scripture 
 
 Guru Granth Sahib Adi Granth Dasam Granth Gurbani Mul Mantar Japji Sahib Chaupai Jaap Sahib Rehras Sukhmani Sahib Tav-Prasad Savaiye 
 
 By country 
 
 Australia Afghanistan Belgium Canada 
 Vancouver 
 Fiji France Germany India Indonesia Iraq Italy Malaysia Nepal Netherlands New Zealand Pakistan Singapore Switzerland Thailand United Arab Emirates United Kingdom United States 
 
 Other topics 
 
 History Gurmukhi alphabet Ik Onkar Waheguru Khanda Gurdwara  ( Harmandir Sahib ) Panj Pyare Literature Music Names Places Politics Nanakshahi calendar Ramananda Fariduddin Ganjshakar Kabir History of the Punjab Sardar Dastar Islam Jainism Hinduism Sikh Empire Mela Maghi Maghi Vaisakhi Hola Mohalla 3HO Sikhs Women in Sikhism Sikhism and sexual orientation Idolatry in Sikhism Criticism Punjab region Punjabi people Punjabi language  ( Gurmukhī ) 
 
 Takht 
 
 Akal Takht Damdama Sahib Kesgarh Sahib Hazur Sahib Patna Sahib 
 
 
   Sikhism portal   
 
 
 
 
 
 v t e 
 
 Calendars 
 Systems 
 
 Lunar Lunisolar Solar 
 
 In wide use 
 
 Astronomical Bengali Chinese Ethiopian Gregorian Hebrew Hindu Iranian Islamic ISO Unix time 
 
 
 In more 
limited use 
 
 
 
 
 Akan Armenian Assyrian Bahá'í   (Badí‘) Balinese pawukon Balinese saka Berber Buddhist Burmese Chinese Coptic Gaelic Germanic Heathen Georgian Hebrew Hindu or Indian 
 Vikram Samvat Saka 
 Igbo Iranian 
 Jalali  (medieval) Hijri  (modern) Zoroastrian 
 Islamic 
 Fasli Tabular 
 Jain Japanese Javanese Korean 
 Juche 
 Kurdish Lithuanian Malayalam Mongolian Melanau Nanakshahi Nepal Sambat Nisg̱a'a Oromo Romanian Somali Sesotho Slavic 
 Slavic Native Faith 
 Tamil Thai 
 lunar solar 
 Tibetan Vietnamese Xhosa Yoruba 
 
 Types 
 
 Runic Mesoamerican 
 Long Count Calendar round 
 
 
 Christian variants 
 
 Julian 
 Revised 
 Liturgical year 
 Eastern Orthodox 
 Saints 
 
 
 Historical 
 
 Attic Aztec 
 Tonalpohualli Xiuhpohualli 
 Babylonian Bulgar Byzantine Celtic Cham Culāsakaraj Egyptian Florentine French Republican Germanic Greek Hindu Inca Macedonian Maya 
 Haab' Tzolk'in 
 Muisca Pentecontad Pisan Rapa Nui Roman calendar Rumi Soviet Swedish Turkmen 
 
 By specialty 
 
 Holocene  (anthropological) Proleptic Gregorian  /  Proleptic Julian  (historiographical) Darian  (Martian) Dreamspell  (New Age) Discordian  /  Pataphysical  (surreal) 
 
 Proposals 
 
 Calendar reform Hanke–Henry Permanent International Fixed Pax Positivist Symmetry454 Tranquility World 
 New Earth Time 
 
 
 Fictional 
 
 Discworld Greyhawk Middle-earth Stardate Star Wars (Galactic Standard Calendar) 
 
 
 Displays and 
applications 
 
 
 Electronic Perpetual Wall 
 
 
 Year naming 
and 
numbering 
 
 
 Terminology 
 
 Era Epoch Regnal name Regnal year Year zero 
 
 Systems 
 
 Ab urbe condita Anno Domini / Common Era Anno Mundi Assyrian Before Present Chinese Imperial Chinese Minguo Human Era Japanese Korean Seleucid Spanish Yugas 
 Satya Treta Dvapara Kali 
 Vietnamese 
 
 
 
 List of calendars 
 
 
 
 
 
 v t e 
 
   State  of  Punjab, India 
 
 Capital :  Chandigarh 
 Topics 
 
 Demographics Economy Education History 
 King Porus 
 People Tourism Music 
 
 
 
 Administration 
 
 Government Legislative Assembly Chief Ministers Governors Raj Bhavan Police 
 
 Culture 
 
 Cinema Cuisine 
 Folk dances Bhangra Giddha Aawat pauni 
 Folklore Punjabi folk religion 
 Sanjhi Gugga Chhapar Mela Sakhi Sarwar Saint Punjabi fasts 
 Bhangala Language 
 Gurmukhī 
 Music 
 Bhangra Folk music 
 Dress
 Salwar (Punjabi) Suit Punjabi ghagra Patiala salwar Punjabi Tamba and Kurta Phulkari Jutti 
 Calendars
 Punjabi calendar Nanakshahi calendar Bikrami calendar 
 Fairs and Festival of Punjab India Punjabi festivals 
 Lohri Basant Kite Festival (Punjab) Maghi Holi, Punjab Teeyan Rakhri Vaisakhi 
 Religious festivals
 Hindu Punjabi Festivals Sikh festivals 
 Sports 
 Kabaddi Kabaddi in India Kila Raipur Sports Festival Punjabi Kabaddi 
 Punjabi Suba movement 
 
 Regions 
 
 Majha Malwa Doaba Powadh 
 
 Districts 
 
 SAS Nagar Sri Amritsar Barnala Bathinda Faridkot Fatehgarh Sahib Fazilka Firozpur Gurdaspur Hoshiarpur Jalandhar Kapurthala Ludhiana Mansa Moga Pathankot Patiala Sri Muktsar Sahib Rupnagar Sangrur Shahid Bhagat Singh Nagar Tarn Taran Sahib 
 
 Major Cities 
 
 Ludhiana Amritsar Jalandhar Patiala Bathinda Hoshiarpur Mohali Batala Pathankot Moga 
 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=Nanakshahi_calendar &oldid=830374927 "					 
				 Categories :  Nanakshahi calendar Specific calendars Hidden categories:  Articles needing additional references from March 2018 All articles needing additional references Articles containing Punjabi-language text 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Article Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read Edit View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 Languages 
			 
								 العربية Français ગુજરાતી हिन्दी Hrvatski Lietuvių മലയാളം मराठी Norsk nynorsk ਪੰਜਾਬੀ پنجابی سنڌي Српски / srpski Srpskohrvatski / српскохрватски தமிழ் తెలుగు اردو 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 14 March 2018, at 12:51. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.340","walltime":"0.419","ppvisitednodes":{"value":1024,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":97985,"limit":2097152},"templateargumentsize":{"value":225,"limit":2097152},"expansiondepth":{"value":10,"limit":40},"expensivefunctioncount":{"value":1,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":10990,"limit":5000000},"entityaccesscount":{"value":0,"limit":400},"timingprofile":["100.00%  309.770      1 -total"," 45.62%  141.311      1 Template:Lang-pa"," 21.69%   67.179      1 Template:Reflist"," 15.97%   49.475      1 Template:Refimprove"," 15.01%   46.487      7 Template:Cite_web "," 10.56%   32.710      6 Template:Navbox"," 10.12%   31.346      1 Template:Ambox","  5.20%   16.099      1 Template:Calendars","  2.92%    9.044      1 Template:SikhCalendar","  1.78%    5.508      1 Template:Sikhism"]},"scribunto":{"limitreport-timeusage":{"value":"0.192","limit":"10.000"},"limitreport-memusage":{"value":12822706,"limit":52428800}},"cachereport":{"origin":"mw1277","timestamp":"20180314125152","ttl":3600,"transientcontent":true}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":71,"wgHostname":"mw1255"});});
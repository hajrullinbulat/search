Suspension bridge 			 
				 From Wikipedia, the free encyclopedia 				 
								 
					Jump to:					 navigation , 					 search 
				 
				 This article is about Suspension bridges with the deck suspended below the main cables. For others, see  Suspension bridge types . 
 
 
 
 This article  needs additional citations for  verification .  Please help  improve this article  by  adding citations to reliable sources . Unsourced material may be challenged and removed.   (May 2012)   ( Learn how and when to remove this template message ) 
 
 Suspension bridge 
 The  Akashi Kaikyō Bridge  in  Japan , world's longest mainspan 
 Ancestor Simple suspension bridge Related Underspanned suspension bridge ; see also  cable-stayed bridge Descendant Self-anchored suspension bridge Carries Pedestrians, bicycles, livestock, automobiles, trucks,  light rail Span range Medium to long Material Steel rope , multiple steel wire strand cables or forged or cast chain links Movable No Design effort medium Falsework  required No 
 
 
 
 
The double-decked  George Washington Bridge , connecting  New York City  to  Bergen County ,  New Jersey , USA, is the world's busiest suspension bridge, carrying 102 million vehicles annually. [1] [2] 
 
 
 A  suspension bridge  is a type of  bridge  in which the deck (the load-bearing portion) is hung below suspension  cables  on vertical suspenders. The first modern examples of this type of bridge were built in the early 19th century. [3] [4]   Simple suspension bridges , which lack vertical suspenders, have a long history in many mountainous parts of the world. 
 This type of bridge has cables suspended between  towers , plus vertical  suspender cables  that carry the weight of the deck below, upon which traffic crosses. This arrangement allows the deck to be level or to arc upward for additional clearance. Like other  suspension bridge types , this type often is constructed without  falsework . 
 The suspension cables must be anchored at each end of the bridge, since any load applied to the bridge is transformed into a tension in these main cables. The main cables continue beyond the pillars to deck-level supports, and further continue to connections with anchors in the ground. The roadway is supported by vertical suspender cables or rods, called hangers. In some circum stances, the towers may sit on a bluff or  canyon  edge where the road may proceed directly to the main span, otherwise the bridge will usually have two smaller spans, running between either pair of pillars and the highway, which may be supported by suspender cables or may use a truss bridge to make this connection. In the latter case there will be very little arc in the outboard main cables. 
 
 
 
 Contents 
 
 1   History 
 1.1   Precursor 1.2   Chain bridges 1.3   Wire-cable 
 2   Structural behavior 
 2.1   Structural analysis 2.2   Advantages 2.3   Disadvantages 
 3   Variations 
 3.1   Underspanned 3.2   Suspension cable types 3.3   Deck structure types 
 4   Forces 
 4.1   Use other than road and rail 
 5   Construction sequence (wire strand cable type) 6   Longest spans 7   Other examples 8   Notable collapses 9   See also 10   References 11   External links 
 
 
 History [ edit ] 
 
 
 
 
The  Manhattan Bridge , connecting  Manhattan  and  Brooklyn  in New York City, opened in 1909 and is considered to be the forerunner of modern suspension bridges; its design served as the model for many of the long-span suspension bridges around the world. 
 
 
 For bridges where the deck follows the suspenders, see  simple suspension bridge . 
 The earliest suspension bridges were ropes slung across a chasm, with a deck possibly at the same level or hung below the ropes such that the rope had a  catenary  shape. 
 Precursor [ edit ] 
 The  Tibetan  saint and bridge-builder  Thangtong Gyalpo  originated the use of  iron  chains in his version of  simple suspension bridges . In 1433, Gyalpo built eight bridges in eastern  Bhutan . The last surviving chain-linked bridge of Gyalpo's was the Thangtong Gyalpo Bridge in  Duksum  en route to  Trashi Yangtse , which was finally washed away in 2004. [5]  Gyalpo's iron chain bridges did not include a  suspended deck bridge  which is the standard on all modern suspension bridges today. Instead, both the railing and the walking layer of Gyalpo's bridges used wires. The  stress points  that carried the screed were reinforced by the iron chains. Before the use of iron chains it is thought that Gyalpo used ropes from twisted willows or yak skins. [6]  He may have also used tightly bound cloth. 
 Chain bridges [ edit ] 
 The first iron chain suspension bridge in the  Western world  was the  Jacob's Creek Bridge  (1801) in  Westmoreland County, Pennsylvania , designed by inventor  James Finley . [7]  Finley's bridge was the first to incorporate all of the necessary components of a modern suspension bridge, including a suspended deck which hung by trusses. Finley patented his design in 1808, and published it in the Philadelphia journal,  The Port Folio , in 1810. [8] 
 
 
 
 
An early plan for the  chain bridge  over the  Menai Strait  near  Bangor, Wales , completed in 1826 
 
 
 Early British chain bridges included the  Dryburgh Abbey Bridge  (1817) and 137 m  Union Bridge  (1820), with spans rapidly increasing to 176 m with the  Menai Bridge  (1826), "the first important modern suspension bridge". [9]  The first chain bridge on the European continent was the  Chain Bridge  in  Nuremberg , Germany. The  Clifton Suspension Bridge  (designed in 1831, completed in 1864 with a 214 m central span) is one of the longest of the parabolic arc chain type. The current  Marlow suspension bridge  was designed by  William Tierney Clark  and was built between 1829 and 1832, replacing a wooden bridge further downstream which collapsed in 1828. It is the only suspension bridge across the non-tidal Thames. The  Széchenyi Chain Bridge , spanning the River Danube in Budapest, was also designed by William Clark and it is a larger scale version of Marlow bridge. [10] 
 An interesting variation is  Thornewill and Warham 's  Ferry Bridge  in  Burton-on-Trent ,  Staffordshire  (1889), where the chains are not attached to abutments as is usual, but instead are attached to the main girders, which are thus in compression. Here, the chains are made from flat wrought iron plates, eight inches (203 mm) wide by an inch and a half (38 mm) thick, rivetted together. [11] 
 Wire-cable [ edit ] 
 The first wire-cable suspension bridge was the  Spider Bridge at Falls of Schuylkill  (1816), a modest and temporary footbridge built following the collapse of James Finley's nearby  Chain Bridge at Falls of Schuylkill  (1808). The footbridge's span was 124 m, although its deck was only 0.45 m wide. 
 Development of wire-cable suspension bridges dates to the temporary simple suspension bridge at  Annonay  built by  Marc Seguin  and his brothers in 1822. It spanned only 18 m. [12]  The first permanent wire cable suspension bridge was  Guillaume Henri Dufour 's Saint Antoine Bridge in  Geneva  of 1823, with two 40 m spans. [12]  The first with cables assembled in mid-air in the modern method was  Joseph Chaley 's Grand Pont Suspendu in  Fribourg , in 1834. [12] 
 In the United States, the first major wire-cable suspension bridge was the  Wire Bridge at Fairmount  in Philadelphia, Pennsylvania. Designed by  Charles Ellet, Jr.  and completed in 1842, it had a span of 109 m. Ellet's  Niagara Falls Suspension Bridge  (1847–48) was abandoned before completion. It was used as  scaffolding  for  John A. Roebling 's  double decker  railroad and carriage bridge (1855). 
 The  Otto Beit Bridge  (1938–39) was the first modern suspension bridge outside the United States built with parallel wire cables. [13] 
 
 
 
 
 
 
 Drawing of the  Tibetan  built Chaksam bridge south of  Lhasa , constructed in 1430, with long chains suspended between towers, and vertical suspender ropes carrying the weight of a planked footway below. 
 
 
 
 
 
 
 
 
 Veranzio 's suspended bridge design (1595) 
 
 
 
 
 
 
 
 
 "View of the Chain Bridge invented by James Finley Esq." (1810) by  William Strickland . Finley's  Chain Bridge at Falls of Schuylkill  (1808) had two spans, 100 feet and 200 feet. 
 
 
 
 
 
 
 
 
 Wire Bridge at Fairmount  (1842, replaced 1874). 
 
 
 
 Structural behavior [ edit ] 
 Structural analysis [ edit ] 
 The main  forces  in a suspension bridge of any type are  tension  in the cables and  compression  in the pillars. Since almost all the force on the pillars is vertically downwards and they are also stabilized by the main cables, the  pillars  can be made quite slender, as on the  Severn Bridge , on the Wales-England border. 
 
 
 
 
 
The slender lines of the  Severn Bridge 
 
 
 In a suspended deck bridge, cables suspended via towers hold up the road deck. The weight is transferred by the cables to the towers, which in turn transfer the weight to the ground. 
 
 
 
 
Comparison of a catenary (black dotted curve) and a parabola (red solid curve) with the same span and sag.
 
 More details 
 The catenary represents the profile of a simple suspension bridge, or the cable of a suspended-deck suspension bridge on which its deck and hangers have negligible mass compared to its cable. The parabola represents the profile of the cable of a suspended-deck suspension bridge on which its cable and hangers have negligible mass compared to its deck. The profile of the cable of a real suspension bridge with the same span and sag lies between the two curves. 
 
 
 
 
 Assuming a negligible weight as compared to the weight of the deck and vehicles being supported, the main cables of a suspension bridge will form a  parabola  (very similar to a  catenary , the form the unloaded cables take before the deck is added). One can see the shape from the constant increase of the gradient of the cable with linear (deck) distance, this increase in gradient at each connection with the deck providing a net upward support force. Combined with the relatively simple constraints placed upon the actual deck, this makes the suspension bridge much simpler to design and analyze than a  cable-stayed bridge , where the deck is in compression. 
 Advantages [ edit ] 
 
 
 
 
A suspension bridge can be made out of simple materials such as wood and common wire rope. 
 
 
 Longer main spans are achievable than with any other type of bridge Less material may be required than other bridge types, even at spans they can achieve, leading to a reduced construction cost Except for installation of the initial temporary cables, little or no access from below is required during construction, for example allowing a waterway to remain open while the bridge is built above May be better able to withstand earthquake movements than heavier and more rigid bridges Bridge decks can have deck sections replaced in order to widen traffic lanes for larger vehicles or add additions width for separated cycling/pedestrian paths. 
 Disadvantages [ edit ] 
 Considerable stiffness or aerodynamic profiling may be required to prevent the bridge deck vibrating under high winds The relatively low deck stiffness compared to other (non-suspension) types of bridges makes it more difficult to carry  heavy rail  traffic where high concentrated  live loads  occur Some access below may be required during construction, to lift the initial cables or to lift deck units. This access can often be avoided in  cable-stayed bridge  construction 
 Variations [ edit ] 
 Underspanned [ edit ] 
 
 
 
 
Micklewood Bridge as illustrated by Charles Drewry, 1832 
 
 
 
 
 
 
Squibb Park Bridge,  Brooklyn , built 2013 
 
 
 
 
 
 
 Eyebar  chain cables of  Clifton Suspension Bridge 
 
 
 
 
 
 
The  Yichang Bridge , a plate deck suspension bridge, over the  Yangtze River  in China 
 
 
 In an underspanned suspension bridge, the main cables hang entirely below the bridge deck, but are still anchored into the ground in a similar way to the conventional type. Very few bridges of this nature have been built, as the deck is inherently less stable than when suspended below the cables. Examples include the Pont des Bergues of 1834 designed by  Guillaume Henri Dufour ; [12]  James Smith's Micklewood Bridge; [14]  and a proposal by  Robert Stevenson  for a bridge over the River Almond near  Edinburgh . [14] 
 Roebling's Delaware Aqueduct  (begun 1847) consists of three sections supported by cables. The timber structure essentially hides the cables; and from a quick view, it is not immediately apparent that it is even a suspension bridge. 
 Suspension cable types [ edit ] 
 The main suspension cables in older bridges were often made from chain or linked bars, but modern bridge cables are made from multiple strands of wire. This not only adds strength but improves reliability (often called redundancy in engineering terms) because the failure of a few flawed strands in the hundreds used pose very little threat of failure, whereas a single bad link or  eyebar  can cause failure of an entire bridge. (The failure of a single eyebar was found to be the cause of the collapse of the  Silver Bridge  over the  Ohio River .) Another reason is that as spans increased, engineers were unable to lift larger chains into position, whereas wire strand cables can be formulated one by one in mid-air from a temporary walkway. 
 Deck structure types [ edit ] 
 Most suspension bridges have open truss structures to support the roadbed, particularly owing to the unfavorable effects of using plate girders, discovered from the  Tacoma Narrows Bridge (1940)  bridge collapse. In the 1960s, developments in bridge aerodynamics allowed the re-introduction of plate structures as shallow  box girders , first seen on the  Severn bridge  built 1961-6. In the picture of the  Yichang Bridge , note the very sharp entry edge and sloping undergirders in the suspension bridge shown. This enables this type of construction to be used without the danger of vortex shedding and consequent aeroelastic effects, such as those that destroyed the original Tacoma Narrows bridge. 
 Forces [ edit ] 
 Three kinds of forces operate on any bridge: the dead load, the live load, and the dynamic load. Dead load refers to the weight of the bridge itself. Like any other structure, a bridge has a tendency to collapse simply because of the gravitational forces acting on the materials of which the bridge is made. Live load refers to traffic that moves across the bridge as well as normal environmental factors such as changes in temperature, precipitation, and winds. Dynamic load refers to environmental factors that go beyond normal weather conditions, factors such as sudden gusts of wind and earthquakes. All three factors must be taken into consideration when building a bridge. 
 Use other than road and rail [ edit ] 
 
 
 
 
Cable-suspended footbridge at  Dallas Fort Worth Airport  Terminal D 
 
 
 The principles of suspension used on the large scale may also appear in contexts less dramatic than road or rail bridges. Light cable suspension may prove less expensive and seem more elegant for a cycle or footbridge than strong girder supports. An example of this is the  Nescio Bridge  in the Netherlands. 
 Where such a bridge spans a gap between two buildings, there is no need to construct special towers, as the buildings can anchor the cables. Cable suspension may also be augmented by the inherent stiffness of a structure that has much in common with a  tubular bridge . 
 
 Construction sequence (wire strand cable type) [ edit ] 
 
 
 
 
The  Little Belt  suspension bridge in  Denmark  was opened in 1970. 
 
 
 
 
 
 
 Manhattan Bridge  in New York City with deck under construction from the towers outward. 
 
 
 
 
 
 
Suspender cables and suspender cable band on the  Golden Gate Bridge  in San Francisco. Main cable diameter is 36 inches (910  mm), and suspender cable diameter is 3.5 inches (89  mm). 
 
 
 
 
 
 
 Lions' Gate Bridge  with deck under construction from the span's center 
 
 
 Typical suspension bridges are constructed using a sequence generally described as follows. Depending on length and size, construction may take anywhere between a year and a half (construction on the original Tacoma Narrows Bridge took only 19 months) up to as long as a decade (the Akashi-Kaikyō Bridge's construction began in May 1986 and was opened in May 1998 – a total of twelve years). 
 Where the towers are founded on underwater piers,  caissons  are sunk and any soft bottom is excavated for a foundation. If the  bedrock  is too deep to be exposed by excavation or the sinking of a caisson, pilings are driven to the bedrock or into overlying hard soil, or a large concrete pad to distribute the weight over less resistant soil may be constructed, first preparing the surface with a bed of compacted gravel. (Such a pad footing can also accommodate the movements of an  active fault , and this has been implemented on the foundations of the  cable-stayed   Rio-Antirio bridge .) The piers are then extended above water level, where they are capped with pedestal bases for the towers. Where the towers are founded on dry land, deep foundation excavation or pilings are used. From the tower foundation, towers of single or multiple columns are erected using high-strength reinforced concrete, stonework, or steel. Concrete is used most frequently in modern suspension bridge construction due to the high cost of steel. Large devices called  saddles , which will carry the main suspension cables, are positioned atop the towers. Typically of cast steel, they can also be manufactured using riveted forms, and are equipped with rollers to allow the main cables to shift under construction and normal loads. Anchorages  are constructed, usually in tandem with the towers, to resist the tension of the cables and form as the main anchor system for the entire structure. These are usually anchored in good quality rock, but may consist of massive reinforced concrete deadweights within an excavation. The anchorage structure will have multiple protruding open  eyebolts  enclosed within a secure space. Temporary suspended walkways, called  catwalks , are then erected using a set of guide wires hoisted into place via winches positioned atop the towers. These catwalks follow the curve set by bridge designers for the main cables, in a path mathematically described as a  catenary  arc. Typical catwalks are usually between eight and ten feet wide, and are constructed using wire grate and wood slats. Gantries are placed upon the catwalks, which will support the main cable spinning reels. Then, cables attached to winches are installed, and in turn, the main cable spinning devices are installed. High strength wire (typically 4 or 6 gauge galvanized steel wire), is pulled in a loop by pulleys on the traveler, with one end affixed at an anchorage. When the traveler reaches the opposite anchorage the loop is placed over an open anchor  eyebar . Along the catwalk, workers also pull the cable wires to their desired tension. This continues until a bundle, called a "cable strand" is completed, and temporarily bundled using stainless steel wire. This process is repeated until the final cable strand is completed. Workers then remove the individual wraps on the cable strands (during the spinning process, the shape of the main cable closely resembles a hexagon), and then the entire cable is then compressed by a traveling hydraulic press into a closely packed cylinder and tightly wrapped with additional wire to form the final circular cross section. The wire used in suspension bridge construction is a galvanized steel wire that has been coated with corrosion inhibitors. At specific points along the main cable (each being the exact distance horizontally in relation to the next) devices called "cable bands" are installed to carry steel wire ropes called  Suspender cables.  Each suspender cable is engineered and cut to precise lengths, and are looped over the cable bands. In some bridges, where the towers are close to or on the shore, the suspender cables may be applied only to the central span. Early suspender cables were fitted with zinc jewels and a set of steel washers, which formed the support for the deck. Modern suspender cables carry a shackle-type fitting. Special lifting hoists attached to the suspenders or from the main cables are used to lift prefabricated sections of bridge deck to the proper level, provided that the local conditions allow the sections to be carried below the bridge by barge or other means. Otherwise, a traveling  cantilever  derrick may be used to extend the deck one section at a time starting from the towers and working outward. If the addition of the deck structure extends from the towers the finished portions of the deck will pitch upward rather sharply, as there is no downward force in the center of the span. Upon completion of the deck the added load will pull the main cables into an arc mathematically described as a  parabola , while the arc of the deck will be as the designer intended – usually a gentle upward arc for added clearance if over a shipping channel, or flat in other cases such as a span over a canyon. Arched suspension spans also give the structure more rigidity and strength. With completion of the primary structure various details such as lighting, handrails, finish painting and paving are installed or completed. 
 Longest spans [ edit ] 
 Main article:  List of longest suspension bridge spans 
 Suspension bridges are typically ranked by the length of their main span. These are the ten bridges with the longest spans, followed by the length of the span and the year the bridge opened for traffic: 
 Akashi Kaikyō Bridge  (Japan), 1991  m (6532  ft) – 1998 Xihoumen Bridge  (China), 1650  m (5413  ft) – 2009 Great Belt Bridge  (Denmark), 1624  m (5328  ft) – 1998 Osman Gazi Bridge  (Turkey),1550  m (5085  ft) – 2016 Yi Sun-sin bridge  (South Korea), 1545  m (5069  ft) – 2012 Runyang Bridge  (China), 1490  m (4888  ft) – 2005 Fourth Nanjing Yangtze Bridge  (China), 1418  m (4652  ft) – 2012 Humber Bridge  (England, United Kingdom), 1410  m (4626  ft) – 1981 (longest span from 1981 until 1998) Yavuz Sultan Selim Bridge  (Turkey), 1408  m (4619  ft) – 2016 Jiangyin Bridge  (China), 1385  m (4544  ft) – 1997 
 Other examples [ edit ] 
 See also:  History of longest vehicle suspension bridge spans 
 (Chronological) 
 Union Bridge  (England/Scotland, 1820), the longest span (137  m) from 1820 to 1826. The oldest in the world still in use today. Roebling's Delaware Aqueduct  (USA, 1847), the oldest wire suspension bridge still in service in United States. John A. Roebling Suspension Bridge  (USA, 1866), then the longest wire suspension bridge in the world at 1,057 feet (322 m) main span. Brooklyn Bridge  (USA, 1883), the first steel-wire suspension bridge. Bear Mountain Bridge  (USA, 1924), the longest suspension span (497  m) from 1924 to 1926. The first suspension bridge to have a concrete deck. The construction methods pioneered in building it would make possible several much larger projects to follow. Ben Franklin Bridge  (Philadelphia, PA, USA, 1926), replaced Bear Mountain Bridge as longest span at 1,750 feet between the towers. Includes an active subway line and never-used trolley stations on the span. [15] San Francisco–Oakland Bay Bridge  (USA, 1936). This was once the longest steel high-level bridge in the world (704  m). [16]  The eastern portion (a  cantilever bridge )  has been replaced  with a  self-anchored suspension bridge  which is the longest of its type in the world. It is also the world's widest bridge. Golden Gate Bridge  (USA, 1937), possibly the most beautiful bridge in the world and the longest suspension bridge from 1937 to 1964. Mackinac Bridge  (USA, 1957), the longest suspension bridge between anchorages in the Western hemisphere. Si Du River Bridge  (China, 2009), the  highest bridge in the world , with its deck around 500 metres above the surface of the river. 
 Notable collapses [ edit ] 
 Silver Bridge ,  Point Pleasant, West Virginia  – Eyebar chain highway bridge, built in 1928, that collapsed in late 1967, killing forty-six people. Tacoma Narrows Bridge , (USA), 853  m – 1940. The  Tacoma Narrows  bridge was vulnerable to structural vibration in sustained and moderately strong winds due to its plate-girder deck structure. Wind caused a phenomenon called aeroelastic fluttering that led to its collapse only months after completion. The collapse was captured on film. No human lives were lost in the collapse; several drivers escaped their cars on foot and reached the anchorages before the span dropped. 
 See also [ edit ] 
 Category:Suspension bridges  — for articles about specific suspension bridges. List of longest suspension bridge spans Timeline of three longest spans  whether bridge,  aerial tramway ,  powerline , ceiling or  dome  etc. Cable-stayed bridge  — superficially similar to a suspension bridge, but cables from the towers directly support the roadway, rather than the road being suspended indirectly by additional cables from the main cables connecting two towers. Inca rope bridge  — has features in common with a suspension bridge and predates them by at least three hundred years. However, in a rope bridge the deck itself is suspended from the anchored piers and the guardrails are non-structural. Self-anchored suspension bridge  — combining elements of a suspension bridge and a cable-stayed bridge. Simple suspension bridge  — a modern implementation of the rope bridge using steel cables, although either the upper guardrail or lower footboard cables may be the main structural cables. 
 References [ edit ] 
 
 
 ^   "Port Authority of New York and New Jersey - George Washington Bridge" . The Port Authority of New York and New Jersey . Retrieved  13 September  2013 .   ^   Bod Woodruff; Lana Zak  & Stephanie Wash (20 November 2012).  "GW Bridge Painters: Dangerous Job on Top of the World's Busiest Bridge" . ABC News . Retrieved  13 September  2013 .   ^   Chakzampa Thangtong Gyalpo – Architect, Philosopher and Iron Chain Bridge Builder  by Manfred Gerner. Thimphu: Center for Bhutan Studies 2007.  ISBN   99936-14-39-4 ^   Lhasa and Its Mysteries  by Lawrence Austine Waddell, 1905, p.313 ^   Bhutan . Lonely Planet. 2007.  ISBN   978-1-74059-529-2 .   ^   "Chakzampa Thangtong Gyalpo"   (PDF) . Centre for Bhutan Studies. p.  61.   ^   "Iron Wire of the Wheeling Suspension Bridge" . Smithsonian Museum Conservation Institute.   ^   Bridges: Three Thousand Years of Defying Nature . MBI Publishing Company. 12 November 2001.  ISBN   978-0-7603-1234-6 .   ^   Encyclopædia Britannica ^   "Marlow Suspension Bridge". Retrieved 11 December 2008. Cove-Smith, Chris (2006). The River Thames Book. Imray Laurie Norie and Wilson.  ISBN   0-85288-892-9 .[page needed]1 ^   https://www.ice.org.uk/disciplines-and-resources/ice-library-and-digital-resources/historical-engineering-works/details?hewID=2746#details ^  a   b   c   d   Peters, Tom F. (1987).  Transitions in Engineering: Guillaume Henri Dufour and the Early 19th Century Cable Suspension Bridges . Birkhauser.  ISBN   3-7643-1929-1 .   ^   Cleveland Bridge Company (UK)  Web site Retrieved 21 February 2007, includes image of the bridge. ^  a   b   Drewry, Charles Stewart (1832).  A Memoir of Suspension Bridges: Comprising The History Of Their Origin And Progress . London: Longman, Rees, Orme, Brown, Green  & Longman . Retrieved  13 June  2009 .   ^   DRPA  :: Delaware River Port Authority ^   McGloin, Bernard.  "Symphonies in Steel: Bay Bridge and the Golden Gate" . Virtual Museum of the City of San Francisco . Retrieved  12 January  2008 .   
 
 
 External links [ edit ] 
 Wikimedia Commons  has media related to:
 Suspension bridges  ( category ) 
 
 Look up  suspension bridge  in Wiktionary, the free dictionary. 
 Historic American Engineering Record  (HAER) No.  NJ-132, " Contextual Essay on Wire Bridges " New Brunswick Canada suspension footbridges Structurae: suspension bridges American Society of Civil Engineers  History and heritage of civil engineering – bridges Bridgemeister: Mostly suspension bridges Wilford, John Noble (8 May 2007).  "How the Inca Leapt Canyons" .  The New York Times .   
 
 
 
 v t e 
 
 Bridge -related articles 
 Structural types 
 
 Arch bridge Bascule bridge Beam bridge Box girder bridge Bridge–tunnel Burr truss Cable-stayed bridge Canopy bridge Cantilever bridge Cantilever spar cable-stayed bridge Covered bridge Crib bridge Extradosed bridge Log bridge Moon bridge Moveable bridge Navigable aqueduct Pile bridge Pontoon bridge  ( Vlotbrug ) Suspension bridge  ( types ) Tilt bridge Timber bridge Through arch bridge Transporter bridge Truss bridge Tubular bridge Viaduct Visual index to various types 
 
 
 
 
 
 Lists of bridges by type 
 
 List of bridges List of road–rail bridges List of bridge–tunnels List of bascule bridges List of multi-level bridges List of toll bridges List of cantilever bridges 
 
 Lists of bridges by size 
 
 By length Suspension bridges Cable-stayed bridges Cantilever bridges Continuous truss bridges Arch bridges Masonry arch bridges Highest Tallest 
 
 Additional lists 
 
 Bridge failures Bridge to nowhere 
 
 
 
   Category   Commons WikiProject   Portal 
 
 
 
 
 Authority control 
 
 GND :  4140629-1 
 
 
 


 
 
 
 
 					 
						Retrieved from " https://en.wikipedia.org/w/index.php?title=Suspension_bridge &oldid=830108630 "					 
				 Categories :  Bridges by structural type Suspension bridges Structural engineering Hidden categories:  Use dmy dates from May 2012 Articles needing additional references from May 2012 All articles needing additional references Wikipedia articles with GND identifiers 				 
							 
		 
		 
			 Navigation menu 
			 
									 
						 Personal tools 
						 Not logged in Talk Contributions Create account Log in 
					 
									 
										 
						 Namespaces 
						 Article Talk 
					 
										 
												 
						 
							 Variants 
						 
						 
							 
						 
					 
									 
				 
										 
						 Views 
						 Read Edit View history 
					 
										 
						 
						 More 
						 
							 
						 
					 
										 
						 
							 Search 
						 
						 
							 
								 							 
						 
					 
									 
			 
			 
				 
						 
			 Navigation 
			 
								 Main page Contents Featured content Current events Random article Donate to Wikipedia Wikipedia store 
							 
		 
			 
			 Interaction 
			 
								 Help About Wikipedia Community portal Recent changes Contact page 
							 
		 
			 
			 Tools 
			 
								 What links here Related changes Upload file Special pages Permanent link Page information Wikidata item Cite this page 
							 
		 
			 
			 Print/export 
			 
								 Create a book Download as PDF Printable version 
							 
		 
			 
			 In other projects 
			 
								 Wikimedia Commons 
							 
		 
			 
			 Languages 
			 
								 Afrikaans العربية Asturianu Azərbaycanca Български Català Čeština Cymraeg Dansk Deutsch Eesti Español Esperanto Euskara فارسی Français Frysk Galego 한국어 हिन्दी Bahasa Indonesia Íslenska Italiano עברית Қазақша Latina Lëtzebuergesch Lietuvių Magyar മലയാളം Bahasa Melayu Nederlands 日本語 Norsk Norsk nynorsk Oʻzbekcha/ўзбекча Polski Português Română Русский Simple English Slovenčina Slovenščina Srpskohrvatski / српскохрватски Suomi Svenska தமிழ் ไทย Türkçe Українська Tiếng Việt 粵語 中文 
				 Edit links 			 
		 
				 
		 
				 
						  This page was last edited on 12 March 2018, at 20:11. Text is available under the  Creative Commons Attribution-ShareAlike License ;
additional terms may apply.  By using this site, you agree to the  Terms of Use  and  Privacy Policy . Wikipedia® is a registered trademark of the  Wikimedia Foundation, Inc. , a non-profit organization. 
						 Privacy policy About Wikipedia Disclaimers Contact Wikipedia Developers Cookie statement Mobile view 
										 
						 					 
						 					 
						 
		 
		 (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.360","walltime":"0.609","ppvisitednodes":{"value":2022,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":47673,"limit":2097152},"templateargumentsize":{"value":2152,"limit":2097152},"expansiondepth":{"value":16,"limit":40},"expensivefunctioncount":{"value":2,"limit":500},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":18450,"limit":5000000},"entityaccesscount":{"value":1,"limit":400},"timingprofile":["100.00%  493.811      1 -total"," 25.02%  123.552      1 Template:Reflist"," 23.16%  114.356      1 Template:Authority_control"," 10.42%   51.479      5 Template:Cite_web","  8.56%   42.274     12 Template :Convert","  7.96%   39.286      1 Template:Refimprove","  6.98%   34.476      1 Template:Ambox","  6.92%   34.158      2 Template:ISBN","  6.08%   30.034      1 Template:Bridge_footer","  5.68%   28.045      1 Template:Navbox"]},"scribunto":{"limitreport-timeusage":{"value":"0.151","limit":"10.000"},"limitreport-memusage":{"value":6294406,"limit":52428800}},"cachereport":{"origin":"mw1341","timestamp":"20180312201138","ttl":1900800,"transientcontent":false}}});}); (window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":85,"wgHostname":"mw1261"});});